package com.prototype.myshopbackend.adapter;

import org.springframework.stereotype.Service;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.PaymentResponse;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundResponse.RefundResponse;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.model.Payment;

public interface NetworkAdapter {

	//deuda tecnica
	PaymentResponse paymentTx(PaymentDTO paymentDTO);

	RefundResponse refundTx(Payment payment);
}
