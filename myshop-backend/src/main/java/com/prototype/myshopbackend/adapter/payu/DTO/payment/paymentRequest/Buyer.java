
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"merchantBuyerId",
		"fullName",
		"emailAddress",
		"contactPhone",
		"dniNumber",
		"shippingAddress"
})
@Generated("jsonschema2pojo")
public class Buyer implements Serializable {

	@JsonProperty("merchantBuyerId")
	private Object merchantBuyerId;
	@JsonProperty("fullName")
	private Object fullName;
	@JsonProperty("emailAddress")
	private String emailAddress;
	@JsonProperty("contactPhone")
	private String contactPhone;
	@JsonProperty("dniNumber")
	private String dniNumber;
	@JsonProperty("shippingAddress")
	private ShippingAddress shippingAddress;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 9037404183651316042L;

	/**
	 * No args constructor for use in serialization
	 */
	public Buyer() {

	}

	/**
	 * @param dniNumber
	 * @param emailAddress
	 * @param merchantBuyerId
	 * @param fullName
	 * @param shippingAddress
	 * @param contactPhone
	 */
	public Buyer(Object merchantBuyerId, Object fullName, String emailAddress, String contactPhone, String dniNumber,
				 ShippingAddress shippingAddress) {

		super();
		this.merchantBuyerId = merchantBuyerId;
		this.fullName = fullName;
		this.emailAddress = emailAddress;
		this.contactPhone = contactPhone;
		this.dniNumber = dniNumber;
		this.shippingAddress = shippingAddress;
	}

	@JsonProperty("merchantBuyerId")
	public Object getMerchantBuyerId() {

		return merchantBuyerId;
	}

	@JsonProperty("merchantBuyerId")
	public void setMerchantBuyerId(Object merchantBuyerId) {

		this.merchantBuyerId = merchantBuyerId;
	}

	@JsonProperty("fullName")
	public Object getFullName() {

		return fullName;
	}

	@JsonProperty("fullName")
	public void setFullName(Object fullName) {

		this.fullName = fullName;
	}

	@JsonProperty("emailAddress")
	public String getEmailAddress() {

		return emailAddress;
	}

	@JsonProperty("emailAddress")
	public void setEmailAddress(String emailAddress) {

		this.emailAddress = emailAddress;
	}

	@JsonProperty("contactPhone")
	public String getContactPhone() {

		return contactPhone;
	}

	@JsonProperty("contactPhone")
	public void setContactPhone(String contactPhone) {

		this.contactPhone = contactPhone;
	}

	@JsonProperty("dniNumber")
	public String getDniNumber() {

		return dniNumber;
	}

	@JsonProperty("dniNumber")
	public void setDniNumber(String dniNumber) {

		this.dniNumber = dniNumber;
	}

	@JsonProperty("shippingAddress")
	public ShippingAddress getShippingAddress() {

		return shippingAddress;
	}

	@JsonProperty("shippingAddress")
	public void setShippingAddress(ShippingAddress shippingAddress) {

		this.shippingAddress = shippingAddress;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(Buyer.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("merchantBuyerId");
		sb.append('=');
		sb.append(((this.merchantBuyerId == null) ? "<null>" : this.merchantBuyerId));
		sb.append(',');
		sb.append("fullName");
		sb.append('=');
		sb.append(((this.fullName == null) ? "<null>" : this.fullName));
		sb.append(',');
		sb.append("emailAddress");
		sb.append('=');
		sb.append(((this.emailAddress == null) ? "<null>" : this.emailAddress));
		sb.append(',');
		sb.append("contactPhone");
		sb.append('=');
		sb.append(((this.contactPhone == null) ? "<null>" : this.contactPhone));
		sb.append(',');
		sb.append("dniNumber");
		sb.append('=');
		sb.append(((this.dniNumber == null) ? "<null>" : this.dniNumber));
		sb.append(',');
		sb.append("shippingAddress");
		sb.append('=');
		sb.append(((this.shippingAddress == null) ? "<null>" : this.shippingAddress));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
