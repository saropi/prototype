
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"language",
		"command",
		"merchant",
		"transaction",
		"test"
})
@Generated("jsonschema2pojo")
public class PaymentRequest implements Serializable {

	@JsonProperty("language")
	private String language;
	@JsonProperty("command")
	private String command;
	@JsonProperty("merchant")
	private Merchant merchant;
	@JsonProperty("transaction")
	private Transaction transaction;
	@JsonProperty("test")
	private boolean test;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -5497256959071812043L;

	/**
	 * No args constructor for use in serialization
	 */

	public PaymentRequest() {

	}

	/**
	 * @param test
	 * @param merchant
	 * @param language
	 * @param command
	 * @param transaction
	 */
	public PaymentRequest(String language, String command, Merchant merchant, Transaction transaction, boolean test) {

		super();
		this.language = language;
		this.command = command;
		this.merchant = merchant;
		this.transaction = transaction;
		this.test = test;
	}

	@JsonProperty("language")
	public String getLanguage() {

		return language;
	}

	@JsonProperty("language")
	public void setLanguage(String language) {

		this.language = language;
	}

	@JsonProperty("command")
	public String getCommand() {

		return command;
	}

	@JsonProperty("command")
	public void setCommand(String command) {

		this.command = command;
	}

	@JsonProperty("merchant")
	public Merchant getMerchant() {

		return merchant;
	}

	@JsonProperty("merchant")
	public void setMerchant(Merchant merchant) {

		this.merchant = merchant;
	}

	@JsonProperty("transaction")
	public Transaction getTransaction() {

		return transaction;
	}

	@JsonProperty("transaction")
	public void setTransaction(Transaction transaction) {

		this.transaction = transaction;
	}

	@JsonProperty("test")
	public boolean isTest() {

		return test;
	}

	@JsonProperty("test")
	public void setTest(boolean test) {

		this.test = test;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(PaymentRequest.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("language");
		sb.append('=');
		sb.append(((this.language == null) ? "<null>" : this.language));
		sb.append(',');
		sb.append("command");
		sb.append('=');
		sb.append(((this.command == null) ? "<null>" : this.command));
		sb.append(',');
		sb.append("merchant");
		sb.append('=');
		sb.append(((this.merchant == null) ? "<null>" : this.merchant));
		sb.append(',');
		sb.append("transaction");
		sb.append('=');
		sb.append(((this.transaction == null) ? "<null>" : this.transaction));
		sb.append(',');
		sb.append("test");
		sb.append('=');
		sb.append(this.test);
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
