
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"street1",
		"street2",
		"city",
		"state",
		"country",
		"postalCode",
		"phone"
})
@Generated("jsonschema2pojo")
public class ShippingAddress implements Serializable {

	@JsonProperty("street1")
	private String street1;
	@JsonProperty("street2")
	private String street2;
	@JsonProperty("city")
	private String city;
	@JsonProperty("state")
	private String state;
	@JsonProperty("country")
	private String country;
	@JsonProperty("postalCode")
	private String postalCode;
	@JsonProperty("phone")
	private String phone;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -7715613772321417307L;

	/**
	 * No args constructor for use in serialization
	 */
	public ShippingAddress() {

	}

	/**
	 * @param country
	 * @param city
	 * @param phone
	 * @param postalCode
	 * @param street1
	 * @param street2
	 * @param state
	 */
	public ShippingAddress(String street1, String street2, String city, String state, String country, String postalCode, String phone) {

		super();
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.phone = phone;
	}

	@JsonProperty("street1")
	public String getStreet1() {

		return street1;
	}

	@JsonProperty("street1")
	public void setStreet1(String street1) {

		this.street1 = street1;
	}

	@JsonProperty("street2")
	public String getStreet2() {

		return street2;
	}

	@JsonProperty("street2")
	public void setStreet2(String street2) {

		this.street2 = street2;
	}

	@JsonProperty("city")
	public String getCity() {

		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {

		this.city = city;
	}

	@JsonProperty("state")
	public String getState() {

		return state;
	}

	@JsonProperty("state")
	public void setState(String state) {

		this.state = state;
	}

	@JsonProperty("country")
	public String getCountry() {

		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {

		this.country = country;
	}

	@JsonProperty("postalCode")
	public String getPostalCode() {

		return postalCode;
	}

	@JsonProperty("postalCode")
	public void setPostalCode(String postalCode) {

		this.postalCode = postalCode;
	}

	@JsonProperty("phone")
	public String getPhone() {

		return phone;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {

		this.phone = phone;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(ShippingAddress.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("street1");
		sb.append('=');
		sb.append(((this.street1 == null) ? "<null>" : this.street1));
		sb.append(',');
		sb.append("street2");
		sb.append('=');
		sb.append(((this.street2 == null) ? "<null>" : this.street2));
		sb.append(',');
		sb.append("city");
		sb.append('=');
		sb.append(((this.city == null) ? "<null>" : this.city));
		sb.append(',');
		sb.append("state");
		sb.append('=');
		sb.append(((this.state == null) ? "<null>" : this.state));
		sb.append(',');
		sb.append("country");
		sb.append('=');
		sb.append(((this.country == null) ? "<null>" : this.country));
		sb.append(',');
		sb.append("postalCode");
		sb.append('=');
		sb.append(((this.postalCode == null) ? "<null>" : this.postalCode));
		sb.append(',');
		sb.append("phone");
		sb.append('=');
		sb.append(((this.phone == null) ? "<null>" : this.phone));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
