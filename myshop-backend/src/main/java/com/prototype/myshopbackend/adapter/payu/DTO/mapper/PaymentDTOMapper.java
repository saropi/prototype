package com.prototype.myshopbackend.adapter.payu.DTO.mapper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.AdditionalValues;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.Buyer;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.CreditCard;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.Merchant;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.Order;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.PaymentRequest;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.ShippingAddress;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.Transaction;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.TxValue;
import com.prototype.myshopbackend.dto.PaymentDTO;

public class PaymentDTOMapper {

	private PaymentDTO paymentDTO;
	private String apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
	private String apiLogin = "pRRXKOl8ikMmt9u";
	Logger log = LoggerFactory.getLogger(PaymentDTOMapper.class);

	public PaymentDTOMapper() {

	}

	public PaymentDTOMapper(PaymentDTO paymentDTO) {

		this.paymentDTO = paymentDTO;
	}

	public Boolean checkPaymentDTO() {

		if (!this.checkCreitCardNumber()) {
			return false;
		}
		if (!this.checkCreitCardSecurityCode()) {
			return false;
		}
		return true;

	}

	;

	private Boolean checkCreitCardNumber() {

		String regex = "[0-9]+";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(this.paymentDTO.getCreditCardNumber());
		log.info("The credit car number is incorrect");
		return m.matches();
	}

	private Boolean checkCreitCardSecurityCode() {

		String regex = "[0-9]+";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(this.paymentDTO.getSecurityCode());
		return m.matches();
	}

	public PaymentRequest mapperToPaymentRequest() {

		PaymentRequest paymentRequest = new PaymentRequest();
		paymentRequest.setLanguage("es");
		paymentRequest.setCommand("SUBMIT_TRANSACTION");
		paymentRequest.setMerchant(this.mapperToMerchant());
		paymentRequest.setTransaction(this.mapperToTransaciton());
		paymentRequest.setTest(true);
		return paymentRequest;

	}

	private Merchant mapperToMerchant() {

		Merchant merchant = new Merchant();
		merchant.setApiKey(this.apiKey);
		merchant.setApiLogin(this.apiLogin);
		return merchant;

	}

	private Transaction mapperToTransaciton() {

		Transaction transaction = new Transaction();
		transaction.setOrder(this.mapperToOrder());
		transaction.setPayer(null);
		transaction.setCreditCard(this.mapperToCreditCard());
		transaction.setType("AUTHORIZATION_AND_CAPTURE");
		transaction.setPaymentMethod("VISA");
		transaction.setPaymentCountry("CO");
		transaction.setDeviceSessionId("0:0:0:0:0:0:0:1");
		transaction.setCookie(null);
		transaction.setUserAgent(
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
		transaction.setReason(null);
		transaction.setParentTransactionId(null);
		return transaction;
	}

	private CreditCard mapperToCreditCard() {

		CreditCard creditCard = new CreditCard();

		creditCard.setNumber(this.paymentDTO.getCreditCardNumber());
		creditCard.setSecurityCode(this.paymentDTO.getSecurityCode());
		creditCard.setExpirationDate(this.paymentDTO.getExpirationDate());
		creditCard.setName(this.paymentDTO.getCreditCardName());

		return creditCard;

	}

	private Order mapperToOrder() {

		Order order = new Order();

		order.setAccountId("512321");
		order.setReferenceCode(this.paymentDTO.getPaymentUuId());
		order.setDescription("HOUSTON");
		order.setLanguage("es");
		order.setSignature(this.hashingMD5(
				"4Vj8eK4rloUd272L48hsrarnUA~508029~" + order.getReferenceCode() + "~" + String.valueOf(this.paymentDTO.getTotalAmount())
						+ "~COP"));
		order.setAdditionalValues(this.mapperToAdditionalValues());
		order.setBuyer(this.mapperToBuyer());

		return order;
	}

	private Buyer mapperToBuyer() {

		Buyer buyer = new Buyer();

		buyer.setMerchantBuyerId(null);
		buyer.setFullName(null);
		buyer.setEmailAddress("approved@test.com");
		buyer.setContactPhone("12312312");
		buyer.setDniNumber("1123123");
		buyer.setShippingAddress(this.mapperToShippingAddress());

		return buyer;
	}

	private ShippingAddress mapperToShippingAddress() {

		ShippingAddress shippingAddress = new ShippingAddress();

		shippingAddress.setStreet1("Calle 12");
		shippingAddress.setStreet2("43");
		shippingAddress.setCity("Meta");
		shippingAddress.setState("Villavicencio");
		shippingAddress.setCountry("CO");
		shippingAddress.setPostalCode("0");
		shippingAddress.setPhone("123123");

		return shippingAddress;
	}

	private AdditionalValues mapperToAdditionalValues() {

		AdditionalValues additionalValues = new AdditionalValues();

		additionalValues.setTxValue(this.mapperToTxValue());

		return additionalValues;
	}

	private TxValue mapperToTxValue() {

		TxValue txValue = new TxValue();

		txValue.setValue(this.paymentDTO.getTotalAmount());
		txValue.setCurrency("COP");

		return txValue;
	}

	private String hashingMD5(String signature) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(signature.getBytes());
			byte[] digest = md.digest();
			return DatatypeConverter.printHexBinary(digest).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			log.error("Encryption algorithm failed ", e);
			return null;
		}

	}
}
