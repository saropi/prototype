
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"accountId",
		"referenceCode",
		"description",
		"language",
		"signature",
		"additionalValues",
		"buyer"
})
@Generated("jsonschema2pojo")
public class Order implements Serializable {

	@JsonProperty("accountId")
	private String accountId;
	@JsonProperty("referenceCode")
	private String referenceCode;
	@JsonProperty("description")
	private String description;
	@JsonProperty("language")
	private String language;
	@JsonProperty("signature")
	private String signature;
	@JsonProperty("additionalValues")
	private AdditionalValues additionalValues;
	@JsonProperty("buyer")
	private Buyer buyer;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -6718191053099749238L;

	/**
	 * No args constructor for use in serialization
	 */
	public Order() {

	}

	/**
	 * @param accountId
	 * @param signature
	 * @param description
	 * @param language
	 * @param referenceCode
	 * @param additionalValues
	 * @param buyer
	 */
	public Order(String accountId, String referenceCode, String description, String language, String signature,
				 AdditionalValues additionalValues, Buyer buyer) {

		super();
		this.accountId = accountId;
		this.referenceCode = referenceCode;
		this.description = description;
		this.language = language;
		this.signature = signature;
		this.additionalValues = additionalValues;
		this.buyer = buyer;
	}

	public String hashingMD5(String signature) throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(signature.getBytes());
		byte[] digest = md.digest();
		return DatatypeConverter.printHexBinary(digest).toUpperCase();
	}

	@JsonProperty("accountId")
	public String getAccountId() {

		return accountId;
	}

	@JsonProperty("accountId")
	public void setAccountId(String accountId) {

		this.accountId = accountId;
	}

	@JsonProperty("referenceCode")
	public String getReferenceCode() {

		return referenceCode;
	}

	@JsonProperty("referenceCode")
	public void setReferenceCode(String referenceCode) {

		this.referenceCode = referenceCode;
	}

	@JsonProperty("description")
	public String getDescription() {

		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {

		this.description = description;
	}

	@JsonProperty("language")
	public String getLanguage() {

		return language;
	}

	@JsonProperty("language")
	public void setLanguage(String language) {

		this.language = language;
	}

	@JsonProperty("signature")
	public String getSignature() {

		return signature;
	}

	@JsonProperty("signature")
	public void setSignature(String signature) {

		this.signature = signature;
	}

	@JsonProperty("additionalValues")
	public AdditionalValues getAdditionalValues() {

		return additionalValues;
	}

	@JsonProperty("additionalValues")
	public void setAdditionalValues(AdditionalValues additionalValues) {

		this.additionalValues = additionalValues;
	}

	@JsonProperty("buyer")
	public Buyer getBuyer() {

		return buyer;
	}

	@JsonProperty("buyer")
	public void setBuyer(Buyer buyer) {

		this.buyer = buyer;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(Order.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("accountId");
		sb.append('=');
		sb.append(((this.accountId == null) ? "<null>" : this.accountId));
		sb.append(',');
		sb.append("referenceCode");
		sb.append('=');
		sb.append(((this.referenceCode == null) ? "<null>" : this.referenceCode));
		sb.append(',');
		sb.append("description");
		sb.append('=');
		sb.append(((this.description == null) ? "<null>" : this.description));
		sb.append(',');
		sb.append("language");
		sb.append('=');
		sb.append(((this.language == null) ? "<null>" : this.language));
		sb.append(',');
		sb.append("signature");
		sb.append('=');
		sb.append(((this.signature == null) ? "<null>" : this.signature));
		sb.append(',');
		sb.append("additionalValues");
		sb.append('=');
		sb.append(((this.additionalValues == null) ? "<null>" : this.additionalValues));
		sb.append(',');
		sb.append("buyer");
		sb.append('=');
		sb.append(((this.buyer == null) ? "<null>" : this.buyer));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
