package com.prototype.myshopbackend.adapter.payu.DTO.mapper;

import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest.Merchant;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest.Order;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest.RefundRequest;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest.Transaction;
import com.prototype.myshopbackend.model.Payment;

public class RefundDTOMapper {

	private Payment payment;
	private String apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
	private String apiLogin = "pRRXKOl8ikMmt9u";

	public RefundDTOMapper() {

	}

	public RefundDTOMapper(Payment payment) {

		this.payment = payment;
	}

	public RefundRequest mapperToRefundRequest() {

		RefundRequest refundRequest = new RefundRequest();

		refundRequest.setLanguage("es");
		refundRequest.setCommand("SUBMIT_TRANSACTION");
		refundRequest.setMerchant(this.mapperToMerchant());
		refundRequest.setTransaction(this.mapperToTransaction());
		refundRequest.setTest(true);
		return refundRequest;
	}

	private Transaction mapperToTransaction() {

		Transaction transaction = new Transaction();
		transaction.setOrder(this.mapperToOrder());
		transaction.setType("REFUND");
		transaction.setReason(" ");
		transaction.setParentTransactionId(this.payment.getTransactionId());
		return transaction;
	}

	private Order mapperToOrder() {

		Order order = new Order();
		order.setId(String.valueOf(this.payment.getPaymentOrderId()));
		return order;
	}

	private Merchant mapperToMerchant() {

		Merchant merchant = new Merchant();
		merchant.setApiKey(this.apiKey);
		merchant.setApiLogin(this.apiLogin);
		return merchant;

	}
}
