
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"value",
		"currency"
})
@Generated("jsonschema2pojo")
public class TxValue implements Serializable {

	@JsonProperty("value")
	private long value;
	@JsonProperty("currency")
	private String currency;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -5181192660790725186L;

	/**
	 * No args constructor for use in serialization
	 */

	public TxValue() {

	}

	/**
	 * @param currency
	 * @param value
	 */
	public TxValue(long value, String currency) {

		super();
		this.value = value;
		this.currency = currency;
	}

	@JsonProperty("value")
	public long getValue() {

		return value;
	}

	@JsonProperty("value")
	public void setValue(long value) {

		this.value = value;
	}

	@JsonProperty("currency")
	public String getCurrency() {

		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(String currency) {

		this.currency = currency;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(TxValue.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("value");
		sb.append('=');
		sb.append(this.value);
		sb.append(',');
		sb.append("currency");
		sb.append('=');
		sb.append(((this.currency == null) ? "<null>" : this.currency));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
