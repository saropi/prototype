
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"orderId",
		"transactionId",
		"state",
		"paymentNetworkResponseCode",
		"paymentNetworkResponseErrorMessage",
		"trazabilityCode",
		"authorizationCode",
		"pendingReason",
		"responseCode",
		"errorCode",
		"responseMessage",
		"transactionDate",
		"transactionTime",
		"operationDate",
		"extraParameters"
})
@Generated("jsonschema2pojo")
public class TransactionResponse implements Serializable {

	@JsonProperty("orderId")
	private long orderId;
	@JsonProperty("transactionId")
	private String transactionId;
	@JsonProperty("state")
	private String state;
	@JsonProperty("paymentNetworkResponseCode")
	private String paymentNetworkResponseCode;
	@JsonProperty("paymentNetworkResponseErrorMessage")
	private Object paymentNetworkResponseErrorMessage;
	@JsonProperty("trazabilityCode")
	private String trazabilityCode;
	@JsonProperty("authorizationCode")
	private Object authorizationCode;
	@JsonProperty("pendingReason")
	private Object pendingReason;
	@JsonProperty("responseCode")
	private String responseCode;
	@JsonProperty("errorCode")
	private Object errorCode;
	@JsonProperty("responseMessage")
	private Object responseMessage;
	@JsonProperty("transactionDate")
	private Object transactionDate;
	@JsonProperty("transactionTime")
	private Object transactionTime;
	@JsonProperty("operationDate")
	private Object operationDate;
	@JsonProperty("extraParameters")
	private Object extraParameters;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -980383916369788815L;

	/**
	 * No args constructor for use in serialization
	 */
	public TransactionResponse() {

	}

	/**
	 * @param operationDate
	 * @param paymentNetworkResponseErrorMessage
	 * @param orderId
	 * @param authorizationCode
	 * @param pendingReason
	 * @param errorCode
	 * @param extraParameters
	 * @param transactionDate
	 * @param transactionTime
	 * @param transactionId
	 * @param responseCode
	 * @param paymentNetworkResponseCode
	 * @param state
	 * @param responseMessage
	 * @param trazabilityCode
	 */
	public TransactionResponse(long orderId, String transactionId, String state, String paymentNetworkResponseCode,
							   Object paymentNetworkResponseErrorMessage, String trazabilityCode, Object authorizationCode,
							   Object pendingReason, String responseCode, Object errorCode, Object responseMessage, Object transactionDate,
							   Object transactionTime, Object operationDate, Object extraParameters) {

		super();
		this.orderId = orderId;
		this.transactionId = transactionId;
		this.state = state;
		this.paymentNetworkResponseCode = paymentNetworkResponseCode;
		this.paymentNetworkResponseErrorMessage = paymentNetworkResponseErrorMessage;
		this.trazabilityCode = trazabilityCode;
		this.authorizationCode = authorizationCode;
		this.pendingReason = pendingReason;
		this.responseCode = responseCode;
		this.errorCode = errorCode;
		this.responseMessage = responseMessage;
		this.transactionDate = transactionDate;
		this.transactionTime = transactionTime;
		this.operationDate = operationDate;
		this.extraParameters = extraParameters;
	}

	@JsonProperty("orderId")
	public long getOrderId() {

		return orderId;
	}

	@JsonProperty("orderId")
	public void setOrderId(long orderId) {

		this.orderId = orderId;
	}

	@JsonProperty("transactionId")
	public String getTransactionId() {

		return transactionId;
	}

	@JsonProperty("transactionId")
	public void setTransactionId(String transactionId) {

		this.transactionId = transactionId;
	}

	@JsonProperty("state")
	public String getState() {

		return state;
	}

	@JsonProperty("state")
	public void setState(String state) {

		this.state = state;
	}

	@JsonProperty("paymentNetworkResponseCode")
	public String getPaymentNetworkResponseCode() {

		return paymentNetworkResponseCode;
	}

	@JsonProperty("paymentNetworkResponseCode")
	public void setPaymentNetworkResponseCode(String paymentNetworkResponseCode) {

		this.paymentNetworkResponseCode = paymentNetworkResponseCode;
	}

	@JsonProperty("paymentNetworkResponseErrorMessage")
	public Object getPaymentNetworkResponseErrorMessage() {

		return paymentNetworkResponseErrorMessage;
	}

	@JsonProperty("paymentNetworkResponseErrorMessage")
	public void setPaymentNetworkResponseErrorMessage(Object paymentNetworkResponseErrorMessage) {

		this.paymentNetworkResponseErrorMessage = paymentNetworkResponseErrorMessage;
	}

	@JsonProperty("trazabilityCode")
	public String getTrazabilityCode() {

		return trazabilityCode;
	}

	@JsonProperty("trazabilityCode")
	public void setTrazabilityCode(String trazabilityCode) {

		this.trazabilityCode = trazabilityCode;
	}

	@JsonProperty("authorizationCode")
	public Object getAuthorizationCode() {

		return authorizationCode;
	}

	@JsonProperty("authorizationCode")
	public void setAuthorizationCode(Object authorizationCode) {

		this.authorizationCode = authorizationCode;
	}

	@JsonProperty("pendingReason")
	public Object getPendingReason() {

		return pendingReason;
	}

	@JsonProperty("pendingReason")
	public void setPendingReason(Object pendingReason) {

		this.pendingReason = pendingReason;
	}

	@JsonProperty("responseCode")
	public String getResponseCode() {

		return responseCode;
	}

	@JsonProperty("responseCode")
	public void setResponseCode(String responseCode) {

		this.responseCode = responseCode;
	}

	@JsonProperty("errorCode")
	public Object getErrorCode() {

		return errorCode;
	}

	@JsonProperty("errorCode")
	public void setErrorCode(Object errorCode) {

		this.errorCode = errorCode;
	}

	@JsonProperty("responseMessage")
	public Object getResponseMessage() {

		return responseMessage;
	}

	@JsonProperty("responseMessage")
	public void setResponseMessage(Object responseMessage) {

		this.responseMessage = responseMessage;
	}

	@JsonProperty("transactionDate")
	public Object getTransactionDate() {

		return transactionDate;
	}

	@JsonProperty("transactionDate")
	public void setTransactionDate(Object transactionDate) {

		this.transactionDate = transactionDate;
	}

	@JsonProperty("transactionTime")
	public Object getTransactionTime() {

		return transactionTime;
	}

	@JsonProperty("transactionTime")
	public void setTransactionTime(Object transactionTime) {

		this.transactionTime = transactionTime;
	}

	@JsonProperty("operationDate")
	public Object getOperationDate() {

		return operationDate;
	}

	@JsonProperty("operationDate")
	public void setOperationDate(Object operationDate) {

		this.operationDate = operationDate;
	}

	@JsonProperty("extraParameters")
	public Object getExtraParameters() {

		return extraParameters;
	}

	@JsonProperty("extraParameters")
	public void setExtraParameters(Object extraParameters) {

		this.extraParameters = extraParameters;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(TransactionResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("orderId");
		sb.append('=');
		sb.append(this.orderId);
		sb.append(',');
		sb.append("transactionId");
		sb.append('=');
		sb.append(((this.transactionId == null) ? "<null>" : this.transactionId));
		sb.append(',');
		sb.append("state");
		sb.append('=');
		sb.append(((this.state == null) ? "<null>" : this.state));
		sb.append(',');
		sb.append("paymentNetworkResponseCode");
		sb.append('=');
		sb.append(((this.paymentNetworkResponseCode == null) ? "<null>" : this.paymentNetworkResponseCode));
		sb.append(',');
		sb.append("paymentNetworkResponseErrorMessage");
		sb.append('=');
		sb.append(((this.paymentNetworkResponseErrorMessage == null) ? "<null>" : this.paymentNetworkResponseErrorMessage));
		sb.append(',');
		sb.append("trazabilityCode");
		sb.append('=');
		sb.append(((this.trazabilityCode == null) ? "<null>" : this.trazabilityCode));
		sb.append(',');
		sb.append("authorizationCode");
		sb.append('=');
		sb.append(((this.authorizationCode == null) ? "<null>" : this.authorizationCode));
		sb.append(',');
		sb.append("pendingReason");
		sb.append('=');
		sb.append(((this.pendingReason == null) ? "<null>" : this.pendingReason));
		sb.append(',');
		sb.append("responseCode");
		sb.append('=');
		sb.append(((this.responseCode == null) ? "<null>" : this.responseCode));
		sb.append(',');
		sb.append("errorCode");
		sb.append('=');
		sb.append(((this.errorCode == null) ? "<null>" : this.errorCode));
		sb.append(',');
		sb.append("responseMessage");
		sb.append('=');
		sb.append(((this.responseMessage == null) ? "<null>" : this.responseMessage));
		sb.append(',');
		sb.append("transactionDate");
		sb.append('=');
		sb.append(((this.transactionDate == null) ? "<null>" : this.transactionDate));
		sb.append(',');
		sb.append("transactionTime");
		sb.append('=');
		sb.append(((this.transactionTime == null) ? "<null>" : this.transactionTime));
		sb.append(',');
		sb.append("operationDate");
		sb.append('=');
		sb.append(((this.operationDate == null) ? "<null>" : this.operationDate));
		sb.append(',');
		sb.append("extraParameters");
		sb.append('=');
		sb.append(((this.extraParameters == null) ? "<null>" : this.extraParameters));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
