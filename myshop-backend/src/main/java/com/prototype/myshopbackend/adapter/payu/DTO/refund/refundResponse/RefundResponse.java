
package com.prototype.myshopbackend.adapter.payu.DTO.refund.refundResponse;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"code",
		"error",
		"transactionResponse"
})
@Generated("jsonschema2pojo")
public class RefundResponse implements Serializable {

	@JsonProperty("code")
	private String code;
	@JsonProperty("error")
	private Object error;
	@JsonProperty("transactionResponse")
	private TransactionResponse transactionResponse;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -2648425002662708456L;

	/**
	 * No args constructor for use in serialization
	 */
	public RefundResponse() {

	}

	/**
	 * @param code
	 * @param transactionResponse
	 * @param error
	 */
	public RefundResponse(String code, Object error, TransactionResponse transactionResponse) {

		super();
		this.code = code;
		this.error = error;
		this.transactionResponse = transactionResponse;
	}

	@JsonProperty("code")
	public String getCode() {

		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {

		this.code = code;
	}

	@JsonProperty("error")
	public Object getError() {

		return error;
	}

	@JsonProperty("error")
	public void setError(Object error) {

		this.error = error;
	}

	@JsonProperty("transactionResponse")
	public TransactionResponse getTransactionResponse() {

		return transactionResponse;
	}

	@JsonProperty("transactionResponse")
	public void setTransactionResponse(TransactionResponse transactionResponse) {

		this.transactionResponse = transactionResponse;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(RefundResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("code");
		sb.append('=');
		sb.append(((this.code == null) ? "<null>" : this.code));
		sb.append(',');
		sb.append("error");
		sb.append('=');
		sb.append(((this.error == null) ? "<null>" : this.error));
		sb.append(',');
		sb.append("transactionResponse");
		sb.append('=');
		sb.append(((this.transactionResponse == null) ? "<null>" : this.transactionResponse));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
