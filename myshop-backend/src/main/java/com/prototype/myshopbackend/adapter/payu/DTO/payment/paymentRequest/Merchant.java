
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"apiKey",
		"apiLogin"
})
@Generated("jsonschema2pojo")
public class Merchant implements Serializable {

	@JsonProperty("apiKey")
	private String apiKey;
	@JsonProperty("apiLogin")
	private String apiLogin;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 2834003135110625029L;

	/**
	 * No args constructor for use in serialization
	 */
	public Merchant() {

	}

	/**
	 * @param apiKey
	 * @param apiLogin
	 */
	public Merchant(String apiKey, String apiLogin) {

		super();
		this.apiKey = apiKey;
		this.apiLogin = apiLogin;
	}

	@JsonProperty("apiKey")
	public String getApiKey() {

		return apiKey;
	}

	@JsonProperty("apiKey")
	public void setApiKey(String apiKey) {

		this.apiKey = apiKey;
	}

	@JsonProperty("apiLogin")
	public String getApiLogin() {

		return apiLogin;
	}

	@JsonProperty("apiLogin")
	public void setApiLogin(String apiLogin) {

		this.apiLogin = apiLogin;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(Merchant.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("apiKey");
		sb.append('=');
		sb.append(((this.apiKey == null) ? "<null>" : this.apiKey));
		sb.append(',');
		sb.append("apiLogin");
		sb.append('=');
		sb.append(((this.apiLogin == null) ? "<null>" : this.apiLogin));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
