package com.prototype.myshopbackend.adapter.payu.adapter;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.prototype.myshopbackend.adapter.NetworkAdapter;
import com.prototype.myshopbackend.adapter.payu.DTO.mapper.RefundDTOMapper;
import com.prototype.myshopbackend.adapter.payu.DTO.mapper.PaymentDTOMapper;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest.PaymentRequest;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.PaymentResponse;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest.RefundRequest;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundResponse.RefundResponse;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.model.Payment;
import lombok.extern.slf4j.Slf4j;

/**
 * PayU Adapter
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */
//@Slf4j
@Service
public class PayUAdapter implements NetworkAdapter {

	/**
	 * PAYU API Url
	 */
	@Value("${paymentProvider.payu.url:'https://sandbox.api.payulatam.com/payments-api/4'}")
	private String payuUrl;

	/**
	 * Template to make rest request
	 */
	private final RestTemplate restTemplate;

	Logger log = LoggerFactory.getLogger(PayUAdapter.class);

	/**
	 * PayU Adapter Constructor
	 *
	 * @param restTemplate
	 */
	public PayUAdapter(  final RestTemplate restTemplate) {

		this.restTemplate = restTemplate;
	}

	/**
	 * Calls the API to make the payment transaction and validates responses
	 *
	 * @param paymentDTO {@link PaymentDTO}
	 */
	@Override
	public PaymentResponse paymentTx(PaymentDTO paymentDTO) {

		PaymentDTOMapper requestDTOMapper = new PaymentDTOMapper(paymentDTO);
		if (!requestDTOMapper.checkPaymentDTO()) {
			log.info("Data is invalid from user: {}", String.valueOf(paymentDTO.getUserId()));
			throw new EmptyResultDataAccessException("Error connecting to PayU API by request from user: " + paymentDTO.getUserId(), 1);
		}

		PaymentRequest paymentRequest = requestDTOMapper.mapperToPaymentRequest();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<PaymentRequest> entity = new HttpEntity<>(paymentRequest, headers);
		ResponseEntity<PaymentResponse> response = restTemplate.postForEntity(payuUrl, entity, PaymentResponse.class);
		//ResponseEntity<PaymentResponse> response = restTemplate.postForEntity("https://sandbox.api.payulatam.com/payments-api/4"
		//																				   + ".0/service.cgi", entity,
		//																	  PaymentResponse.class);

		return response.getBody();

	}

	/**
	 * Calls the API to make the payment refund transaction and validates responses
	 *
	 * @param payment {@link Payment}
	 */
	@Override
	public RefundResponse refundTx(Payment payment) {

		RefundRequest refundRequest;
		try {
			RefundDTOMapper refundDTOMapper = new RefundDTOMapper(payment);
			refundRequest = refundDTOMapper.mapperToRefundRequest();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

			HttpEntity<RefundRequest> entity = new HttpEntity<>(refundRequest, headers);
			ResponseEntity<RefundResponse> response = this.restTemplate.postForEntity(this.payuUrl, entity, RefundResponse.class);
			//ResponseEntity<RefundResponse> response = this.restTemplate.postForEntity("https://sandbox.api.payulatam.com/payments-api/4"
			//																				   + ".0/service.cgi", entity,
			//																		  RefundResponse.class);

			return response.getBody();
		} catch (RestClientException e) {
			log.error("Error connecting to PayU API by request from user {}", payment.getUserId());
			throw new EmptyResultDataAccessException("Error connecting to PayU API by request from user: " + payment.getUserId(), 1);
		}
	}
}
