
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"TX_VALUE"
})
@Generated("jsonschema2pojo")
public class AdditionalValues implements Serializable {

	@JsonProperty("TX_VALUE")
	private TxValue txValue;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -6630379028508429185L;

	/**
	 * No args constructor for use in serialization
	 */
	public AdditionalValues() {

	}

	/**
	 * @param txValue
	 */
	public AdditionalValues(TxValue txValue) {

		super();
		this.txValue = txValue;
	}

	@JsonProperty("TX_VALUE")
	public TxValue getTxValue() {

		return txValue;
	}

	@JsonProperty("TX_VALUE")
	public void setTxValue(TxValue txValue) {

		this.txValue = txValue;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(AdditionalValues.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("txValue");
		sb.append('=');
		sb.append(((this.txValue == null) ? "<null>" : this.txValue));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
