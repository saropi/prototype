
package com.prototype.myshopbackend.adapter.payu.DTO.refund.refundRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.model.Payment;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"order",
		"type",
		"reason",
		"parentTransactionId"
})
@Generated("jsonschema2pojo")
public class Transaction implements Serializable {

	@JsonProperty("order")
	private Order order;
	@JsonProperty("type")
	private String type;
	@JsonProperty("reason")
	private String reason;
	@JsonProperty("parentTransactionId")
	private String parentTransactionId;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -5470177155274024547L;

	/**
	 * No args constructor for use in serialization
	 */
	public Transaction() {

	}

	/**
	 * @param reason
	 * @param type
	 * @param order
	 * @param parentTransactionId
	 */
	public Transaction(Order order, String type, String reason, String parentTransactionId) {

		super();
		this.order = order;
		this.type = type;
		this.reason = reason;
		this.parentTransactionId = parentTransactionId;
	}

	@JsonProperty("order")
	public Order getOrder() {

		return order;
	}

	@JsonProperty("order")
	public void setOrder(Order order) {

		this.order = order;
	}

	@JsonProperty("type")
	public String getType() {

		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {

		this.type = type;
	}

	@JsonProperty("reason")
	public String getReason() {

		return reason;
	}

	@JsonProperty("reason")
	public void setReason(String reason) {

		this.reason = reason;
	}

	@JsonProperty("parentTransactionId")
	public String getParentTransactionId() {

		return parentTransactionId;
	}

	@JsonProperty("parentTransactionId")
	public void setParentTransactionId(String parentTransactionId) {

		this.parentTransactionId = parentTransactionId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(Transaction.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("order");
		sb.append('=');
		sb.append(((this.order == null) ? "<null>" : this.order));
		sb.append(',');
		sb.append("type");
		sb.append('=');
		sb.append(((this.type == null) ? "<null>" : this.type));
		sb.append(',');
		sb.append("reason");
		sb.append('=');
		sb.append(((this.reason == null) ? "<null>" : this.reason));
		sb.append(',');
		sb.append("parentTransactionId");
		sb.append('=');
		sb.append(((this.parentTransactionId == null) ? "<null>" : this.parentTransactionId));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
