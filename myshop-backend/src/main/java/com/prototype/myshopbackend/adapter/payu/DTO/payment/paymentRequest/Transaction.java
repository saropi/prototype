
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"order",
		"payer",
		"creditCard",
		"type",
		"paymentMethod",
		"paymentCountry",
		"deviceSessionId",
		"ipAddress",
		"cookie",
		"userAgent",
		"reason",
		"parentTransactionId"
})
@Generated("jsonschema2pojo")
public class Transaction implements Serializable {

	@JsonProperty("order")
	private Order order;
	@JsonProperty("payer")
	private Object payer;
	@JsonProperty("creditCard")
	private CreditCard creditCard;
	@JsonProperty("type")
	private String type;
	@JsonProperty("paymentMethod")
	private String paymentMethod;
	@JsonProperty("paymentCountry")
	private String paymentCountry;
	@JsonProperty("deviceSessionId")
	private String deviceSessionId;
	@JsonProperty("ipAddress")
	private String ipAddress;
	@JsonProperty("cookie")
	private Object cookie;
	@JsonProperty("userAgent")
	private String userAgent;
	@JsonProperty("reason")
	private Object reason;
	@JsonProperty("parentTransactionId")
	private Object parentTransactionId;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -7467470590868466L;

	/**
	 * No args constructor for use in serialization
	 */
	public Transaction() {

	}

	/**
	 * @param reason
	 * @param deviceSessionId
	 * @param cookie
	 * @param ipAddress
	 * @param paymentMethod
	 * @param userAgent
	 * @param creditCard
	 * @param type
	 * @param payer
	 * @param paymentCountry
	 * @param order
	 * @param parentTransactionId
	 */
	public Transaction(Order order, Object payer, CreditCard creditCard, String type, String paymentMethod, String paymentCountry,
					   String deviceSessionId, String ipAddress, Object cookie, String userAgent, Object reason,
					   Object parentTransactionId) {

		super();
		this.order = order;
		this.payer = payer;
		this.creditCard = creditCard;
		this.type = type;
		this.paymentMethod = paymentMethod;
		this.paymentCountry = paymentCountry;
		this.deviceSessionId = deviceSessionId;
		this.ipAddress = ipAddress;
		this.cookie = cookie;
		this.userAgent = userAgent;
		this.reason = reason;
		this.parentTransactionId = parentTransactionId;
	}

	@JsonProperty("order")
	public Order getOrder() {

		return order;
	}

	@JsonProperty("order")
	public void setOrder(Order order) {

		this.order = order;
	}

	@JsonProperty("payer")
	public Object getPayer() {

		return payer;
	}

	@JsonProperty("payer")
	public void setPayer(Object payer) {

		this.payer = payer;
	}

	@JsonProperty("creditCard")
	public CreditCard getCreditCard() {

		return creditCard;
	}

	@JsonProperty("creditCard")
	public void setCreditCard(CreditCard creditCard) {

		this.creditCard = creditCard;
	}

	@JsonProperty("type")
	public String getType() {

		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {

		this.type = type;
	}

	@JsonProperty("paymentMethod")
	public String getPaymentMethod() {

		return paymentMethod;
	}

	@JsonProperty("paymentMethod")
	public void setPaymentMethod(String paymentMethod) {

		this.paymentMethod = paymentMethod;
	}

	@JsonProperty("paymentCountry")
	public String getPaymentCountry() {

		return paymentCountry;
	}

	@JsonProperty("paymentCountry")
	public void setPaymentCountry(String paymentCountry) {

		this.paymentCountry = paymentCountry;
	}

	@JsonProperty("deviceSessionId")
	public String getDeviceSessionId() {

		return deviceSessionId;
	}

	@JsonProperty("deviceSessionId")
	public void setDeviceSessionId(String deviceSessionId) {

		this.deviceSessionId = deviceSessionId;
	}

	@JsonProperty("ipAddress")
	public String getIpAddress() {

		return ipAddress;
	}

	@JsonProperty("ipAddress")
	public void setIpAddress(String ipAddress) {

		this.ipAddress = ipAddress;
	}

	@JsonProperty("cookie")
	public Object getCookie() {

		return cookie;
	}

	@JsonProperty("cookie")
	public void setCookie(Object cookie) {

		this.cookie = cookie;
	}

	@JsonProperty("userAgent")
	public String getUserAgent() {

		return userAgent;
	}

	@JsonProperty("userAgent")
	public void setUserAgent(String userAgent) {

		this.userAgent = userAgent;
	}

	@JsonProperty("reason")
	public Object getReason() {

		return reason;
	}

	@JsonProperty("reason")
	public void setReason(Object reason) {

		this.reason = reason;
	}

	@JsonProperty("parentTransactionId")
	public Object getParentTransactionId() {

		return parentTransactionId;
	}

	@JsonProperty("parentTransactionId")
	public void setParentTransactionId(Object parentTransactionId) {

		this.parentTransactionId = parentTransactionId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(Transaction.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("order");
		sb.append('=');
		sb.append(((this.order == null) ? "<null>" : this.order));
		sb.append(',');
		sb.append("payer");
		sb.append('=');
		sb.append(((this.payer == null) ? "<null>" : this.payer));
		sb.append(',');
		sb.append("creditCard");
		sb.append('=');
		sb.append(((this.creditCard == null) ? "<null>" : this.creditCard));
		sb.append(',');
		sb.append("type");
		sb.append('=');
		sb.append(((this.type == null) ? "<null>" : this.type));
		sb.append(',');
		sb.append("paymentMethod");
		sb.append('=');
		sb.append(((this.paymentMethod == null) ? "<null>" : this.paymentMethod));
		sb.append(',');
		sb.append("paymentCountry");
		sb.append('=');
		sb.append(((this.paymentCountry == null) ? "<null>" : this.paymentCountry));
		sb.append(',');
		sb.append("deviceSessionId");
		sb.append('=');
		sb.append(((this.deviceSessionId == null) ? "<null>" : this.deviceSessionId));
		sb.append(',');
		sb.append("ipAddress");
		sb.append('=');
		sb.append(((this.ipAddress == null) ? "<null>" : this.ipAddress));
		sb.append(',');
		sb.append("cookie");
		sb.append('=');
		sb.append(((this.cookie == null) ? "<null>" : this.cookie));
		sb.append(',');
		sb.append("userAgent");
		sb.append('=');
		sb.append(((this.userAgent == null) ? "<null>" : this.userAgent));
		sb.append(',');
		sb.append("reason");
		sb.append('=');
		sb.append(((this.reason == null) ? "<null>" : this.reason));
		sb.append(',');
		sb.append("parentTransactionId");
		sb.append('=');
		sb.append(((this.parentTransactionId == null) ? "<null>" : this.parentTransactionId));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
