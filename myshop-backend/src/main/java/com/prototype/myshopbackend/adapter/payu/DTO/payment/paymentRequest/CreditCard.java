
package com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.prototype.myshopbackend.dto.PaymentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"number",
		"securityCode",
		"expirationDate",
		"name"
})
@Generated("jsonschema2pojo")
public class CreditCard implements Serializable {

	@JsonProperty("number")
	private String number;
	@JsonProperty("securityCode")
	private String securityCode;
	@JsonProperty("expirationDate")
	private String expirationDate;
	@JsonProperty("name")
	private String name;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -383306944507971712L;

	/**
	 * No args constructor for use in serialization
	 */
	public CreditCard() {

	}

	/**
	 * @param number
	 * @param name
	 * @param securityCode
	 * @param expirationDate
	 */
	public CreditCard(String number, String securityCode, String expirationDate, String name) {

		super();
		this.number = number;
		this.securityCode = securityCode;
		this.expirationDate = expirationDate;
		this.name = name;
	}

	@JsonProperty("number")
	public String getNumber() {

		return number;
	}

	@JsonProperty("number")
	public void setNumber(String number) {

		this.number = number;
	}

	@JsonProperty("securityCode")
	public String getSecurityCode() {

		return securityCode;
	}

	@JsonProperty("securityCode")
	public void setSecurityCode(String securityCode) {

		this.securityCode = securityCode;
	}

	@JsonProperty("expirationDate")
	public String getExpirationDate() {

		return expirationDate;
	}

	@JsonProperty("expirationDate")
	public void setExpirationDate(String expirationDate) {

		this.expirationDate = expirationDate;
	}

	@JsonProperty("name")
	public String getName() {

		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {

		this.name = name;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {

		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {

		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(CreditCard.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("number");
		sb.append('=');
		sb.append(((this.number == null) ? "<null>" : this.number));
		sb.append(',');
		sb.append("securityCode");
		sb.append('=');
		sb.append(((this.securityCode == null) ? "<null>" : this.securityCode));
		sb.append(',');
		sb.append("expirationDate");
		sb.append('=');
		sb.append(((this.expirationDate == null) ? "<null>" : this.expirationDate));
		sb.append(',');
		sb.append("name");
		sb.append('=');
		sb.append(((this.name == null) ? "<null>" : this.name));
		sb.append(',');
		sb.append("additionalProperties");
		sb.append('=');
		sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}
