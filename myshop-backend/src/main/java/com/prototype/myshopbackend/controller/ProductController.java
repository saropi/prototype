package com.prototype.myshopbackend.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prototype.myshopbackend.model.Product;
import com.prototype.myshopbackend.service.PaymentService;
import com.prototype.myshopbackend.service.ProductService;
import com.prototype.myshopbackend.service.ShoppingCarService;

/**
 * Product Controller
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@RestController
@RequestMapping("/product")
public class ProductController {

	/**
	 * Product Service
	 */
	private final ProductService productService;

	/**
	 * Product Controller Constructor
	 *
	 * @param productService {@link ProductService}
	 */
	public ProductController(final ProductService productService) {

		this.productService = productService;
	}

	/**
	 * Gets all the products
	 */
	@GetMapping("/all")
	public ResponseEntity<List<Product>> getAllProducts() {

		List<Product> products = productService.findAllProduct();
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	/**
	 * Gets a product given a product Id
	 *
	 * @param id
	 */
	@GetMapping("/find/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable("id") int id) {

		Product product = productService.findProductById(id);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	/**
	 * Adds a product
	 *
	 * @param product {@link Product}
	 */
	@PostMapping("/addProduct")
	public ResponseEntity<Product> addProduct(@RequestBody Product product) {

		Product newProduct = productService.addProduct(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	/**
	 * Updates a product
	 *
	 * @param product {@link Product}
	 */
	@PostMapping("/updateProduct")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product) {

		Product newProduct = productService.updateProduct(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	/**
	 * Deletes a product given an Id
	 *
	 * @param id the id of the product
	 */
	@GetMapping("/deleteProduct/{id}")
	public ResponseEntity<Product> deleteProduct(@PathVariable("id") int id) {

		productService.deleteProductById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
