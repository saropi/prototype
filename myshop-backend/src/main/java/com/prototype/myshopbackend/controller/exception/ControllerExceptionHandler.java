package com.prototype.myshopbackend.controller.exception;
import java.time.LocalDateTime;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ResponseEntity<ApiError> handleEmptyResultDataAccessException(EmptyResultDataAccessException exception){
		ApiError responseBody = ApiError.builder()
										.message(exception.getMessage())
										.timestamp(LocalDateTime.now())
										.status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
										.build();
		return ResponseEntity.status(500).body(responseBody);
	}
}
