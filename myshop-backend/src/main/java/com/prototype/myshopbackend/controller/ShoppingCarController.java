package com.prototype.myshopbackend.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prototype.myshopbackend.model.Product;
import com.prototype.myshopbackend.model.ShoppingCar;
import com.prototype.myshopbackend.service.ProductService;
import com.prototype.myshopbackend.service.ShoppingCarService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shopping Car Controller
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@RestController
@RequestMapping("/shoppingCar")
public class ShoppingCarController {

	/**
	 * Shopping car service Product service
	 */
	private final ShoppingCarService carService;
	private final ProductService productService;
	Logger log = LoggerFactory.getLogger(ShoppingCarController.class);

	/**
	 * Shopping Car Controller Constructor
	 *
	 * @param carService     {@link ShoppingCarService}
	 * @param productService {@link ProductService}
	 */
	public ShoppingCarController(final ShoppingCarService carService, final ProductService productService) {

		this.carService = carService;
		this.productService = productService;
	}

	/**
	 * Gets all the cars in the system
	 */
	@GetMapping("/all")
	public ResponseEntity<List<ShoppingCar>> getAllCars() {

		List<ShoppingCar> cars = carService.findAllCars();
		return new ResponseEntity<List<ShoppingCar>>(cars, HttpStatus.OK);
	}

	/**
	 * Gets an specific car given an Id
	 *
	 * @param id
	 */
	@GetMapping("/find/{id}")
	public ResponseEntity<ShoppingCar> getCarById(@PathVariable("id") int id) {

		ShoppingCar car = carService.findShoppingCarById(id);
		return new ResponseEntity<ShoppingCar>(car, HttpStatus.OK);
	}

	/**
	 * Adds an specific product to a specific car
	 *
	 * @param carId
	 * @param product {@link Product}
	 */
	@PostMapping("/addProductToCar/{carId}")
	public ResponseEntity<ShoppingCar> addProductToCar(@PathVariable(name = "carId") int carId,
													   @RequestBody Product product) {

		ShoppingCar newCar = carService.findShoppingCarById(carId);
		ShoppingCar response = carService.addProductToCar(newCar, product);
		return new ResponseEntity<ShoppingCar>(response, HttpStatus.OK);
	}

	/**
	 * Deletes an specific product to a specific car
	 *
	 * @param carId
	 * @param product {@link Product}
	 */
	@PostMapping("/deleteProductFromCar/{carId}")
	public ResponseEntity<ShoppingCar> deleteProductFromCar(@PathVariable(name = "carId") int carId,
															@RequestBody Product product) {

		Product newP = productService.findProductById(product.getProductId());
		ShoppingCar newCar = carService.findShoppingCarById(carId);
		ShoppingCar response = carService.deleteProductFromCar(newCar, newP);
		return new ResponseEntity<ShoppingCar>(response, HttpStatus.OK);
	}

	/**
	 * Deletes an specific car
	 *
	 * @param id
	 */
	@DeleteMapping("/deleteShoppingCar/{id}")
	public ResponseEntity<ShoppingCar> deleteProduct(@PathVariable("id") int id) {

		carService.deleteShoppingCarById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
