package com.prototype.myshopbackend.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prototype.myshopbackend.model.ShoppingCar;
import com.prototype.myshopbackend.model.User;
import com.prototype.myshopbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User Controller
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@RestController
@RequestMapping("/user")
public class UserController {

	/**
	 * User service
	 */
	private final UserService userService;
	Logger log = LoggerFactory.getLogger(UserController.class);

	/**
	 * Shopping Car Controller Constructor
	 *
	 * @param userService {@link UserService}
	 */
	public UserController(final UserService userService) {

		this.userService = userService;
	}

	/**
	 * login
	 *
	 * @param user {@link User}
	 */
	@PostMapping("/login")
	public ResponseEntity<User> checkUserByDocumentPassword(@RequestBody User user) throws Exception {

		String document = user.getUserDocument();
		String password = user.getUserPassword();
		if (document != null && password != null) {
			User tempUser = userService.findUserByDocument(document);
			if (user == null) {
				log.warn("Bad credentials");
				throw new Exception("Bad credentials");
			}
			if (tempUser.getUserPassword().equals(password)) {
				log.info("The user {} is loggin in", String.valueOf(user.getUserId()));
				return new ResponseEntity<User>(tempUser, HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	/**
	 * Gets all the user in the system
	 */
	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsers() {

		List<User> users = userService.findAllUsers();
		log.info("Getting user list");
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	/**
	 * Creates a car given a user if it does not exist, otherwise returns the car
	 *
	 * @param userId
	 * @param car    {@link ShoppingCar}
	 */
	@PostMapping("/addCar/{userId}")
	public ResponseEntity<User> addCar(@PathVariable("userId") int userId, @RequestBody ShoppingCar car) {

		User user = userService.findUserById(userId);
		if (user.getCar() == null) {
			user.setCar(car);
			log.info("A new car has just been joined to the user {}", String.valueOf(user.getUserId()));
		}
		User newU = userService.updateUser(user);
		return new ResponseEntity<User>(newU, HttpStatus.CREATED);
	}

	/**
	 * Gets a user given an ID
	 *
	 * @param id
	 */
	@GetMapping("/find/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") int id) {

		User user = userService.findUserById(id);
		log.info("get the user {}", String.valueOf(user.getUserId()));
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * Creates a new user. If exists return exception
	 *
	 * @param user {@link User}
	 */
	@PostMapping("/addUser")
	public ResponseEntity<User> addUser(@RequestBody User user) throws Exception {

		String tempDocument = user.getUserDocument();
		if (tempDocument != null && !"".equals(tempDocument)) {
			User newUser = userService.findUserByDocument(tempDocument);
			if (newUser != null) {
				log.warn("The user by document {} already exists", user.getUserDocument());
				throw new Exception("User with " + tempDocument + " already exist");
			}
		}
		User newUser = userService.addUser(user);
		log.info("New user has been created: {}", String.valueOf(newUser.getUserId()));
		return new ResponseEntity<User>(newUser, HttpStatus.CREATED);
	}

	/**
	 * Updates a new user. If exists return exception
	 *
	 * @param user {@link User}
	 */
	@PutMapping("/updateUser")
	public ResponseEntity<User> updateUser(@RequestBody User user) {

		User newUser = userService.updateUser(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * Updates a new user. If exists return exception
	 *
	 * @param id
	 */
	@DeleteMapping("/deleteUser/{id}")
	public ResponseEntity<User> deleteUser(@PathVariable("id") int id) {

		userService.deleteUserById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
