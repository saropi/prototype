package com.prototype.myshopbackend.controller.exception;

import java.time.LocalDateTime;
import java.util.Collection;

import lombok.Builder;
import lombok.Data;

/**
 * Model that represents in a deeper way why and how an {@link ApiError} is fired,
 * explaining in which field the error happened and the rejected value
 *
 * @author <a href="mailto:nicolas.garcia@payu.com"> Nicolas Garcia </a>
 * @since 1.0.0
 */
@Data
@Builder
public class ApiError {

	/**
	 * The http status for the api error
	 */
	private String status;

	/**
	 * Time stamp in which the error happened
	 */
	private LocalDateTime timestamp;

	/**
	 * A human-readable message with the error details
	 */
	private String message;

	/**
	 * Deeper details around the error
	 */
	private String debugMessage;

	/**
	 * If the api error is a summary of errors, the sub errors field
	 * represent all the error that happened at the same time that
	 * prevent a correct processing into the controllers layer.
	 */
}
