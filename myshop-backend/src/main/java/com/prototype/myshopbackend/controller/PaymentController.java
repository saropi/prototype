package com.prototype.myshopbackend.controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.model.Payment;
import com.prototype.myshopbackend.service.PaymentService;
import com.prototype.myshopbackend.service.ShoppingCarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Payment Controller
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@RestController
@RequestMapping("/payment")
public class PaymentController {
	/**
	 * Payment Service
	 * Shopping car service
	 */
	private final PaymentService paymentService;
	private final ShoppingCarService carService;
	Logger log = LoggerFactory.getLogger(PaymentController.class);

	/**
	 * Payment Controller Constructor
	 * @param paymentService {@link PaymentService}
	 * @param carService {@link ShoppingCarService}
	 */
	public PaymentController(final PaymentService paymentService, final ShoppingCarService carService) {
		this.paymentService = paymentService;
		this.carService = carService;
	}

	/**
	 * Get all the payments by a specific user
	 * @param userId the id of the user
	 */
	@GetMapping("/all/{userId}")
	public ResponseEntity<List<Payment>> getAllPayments(@PathVariable("userId") int userId){
		List<Payment> payments = paymentService.findAllpaymentsByUserId(userId);
		return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
	}

	/**
	 * This makes a payment of a car and call the Houston API
	 * @param carId the id of the car
	 * @param paymentDTO {@link PaymentDTO}
	 */
	@PostMapping("/pay/{carId}")
	public ResponseEntity<Payment> generatePayment(@PathVariable("carId") int carId,@RequestBody PaymentDTO paymentDTO){

		Payment response = paymentService.addPayment(paymentDTO);
		if(response == null){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(!"DECLINED".equals(response.getPaymentStatus())){
			carService.clearCar(carService.findShoppingCarById(carId));
			return new ResponseEntity<Payment>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Payment>(response, HttpStatus.OK);

	}

	/**
	 * This makes a refund of a payment and call the Houston API
	 * @param payment {@link Payment}
	 */
	@PostMapping("/refund")
	public ResponseEntity<Payment> generateRefund(@RequestBody Payment payment){
		Payment newPayment = paymentService.findPaymentById(payment.getPaymentId());
		Payment response = paymentService.refund(newPayment);
		if( response == null){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Payment>(response, HttpStatus.OK);
	}

	/**
	 * This returns a payment given an ID
	 * @param id the id of an specific payment
	 */
	@GetMapping("/find/{id}")
	public ResponseEntity<Payment> getPaymentById(@PathVariable("id") int id){
		Payment payment = paymentService.findPaymentById(id);
		return new ResponseEntity<Payment>(payment, HttpStatus.OK);
	}

}
