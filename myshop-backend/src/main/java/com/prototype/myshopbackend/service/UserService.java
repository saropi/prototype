package com.prototype.myshopbackend.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.prototype.myshopbackend.exception.UserNotFoundException;
import com.prototype.myshopbackend.model.Product;
import com.prototype.myshopbackend.model.User;
import com.prototype.myshopbackend.repository.ShoppingCarRepository;
import com.prototype.myshopbackend.repository.UserRepository;


/**
 * User Service
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@Service
public class UserService {
	/**
	 * The user repo
	 */
    private final UserRepository userRepo;

	/**
	 * Product service Constructor
	 * @param userRepo {@link UserRepository}
	 */
    public  UserService(UserRepository userRepo) {
    	this.userRepo=userRepo;
    }

	/**
	 * Adds a product
	 * @param user {@link User}
	 */
    public User addUser(User user) {
    	return userRepo.save(user);
    }

	/**
	 * Gets all te users  a product
	 */
    public List<User> findAllUsers(){
    	return userRepo.findAll();
	}

	/**
	 * Updates a product
	 * @param user {@link User}
	 */
	public User updateUser(User user) {
		return userRepo.save(user);
	}

	/**
	 * deletes a user by id
	 * @param id
	 */
	public void deleteUserById(Integer id) {
		userRepo.deleteById(id);
	}

	/**
	 * Gets a user by a given ID
	 * @param id
	 */
	public User findUserById(Integer id) {
		return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException("User not by id: " + id +" not found"));
	}

	/**
	 * Gets a user by a given user document
	 * @param document
	 */
	public User findUserByDocument(String document) {
		return userRepo.findByUserDocument(document);
	}

}
