package com.prototype.myshopbackend.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.prototype.myshopbackend.exception.ProductNotFoundException;
import com.prototype.myshopbackend.model.Product;
import com.prototype.myshopbackend.model.ShoppingCar;
import com.prototype.myshopbackend.repository.ShoppingCarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shopping Car Service
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@Service
public class ShoppingCarService {

	/**
	 * Shopping car repository
	 */
	private final ShoppingCarRepository carRepository;
	Logger log = LoggerFactory.getLogger(ShoppingCarService.class);

	/**
	 * Product service Constructor
	 * @param carRepository {@link ShoppingCarRepository}
	 */
	public ShoppingCarService(final ShoppingCarRepository carRepository) {
		this.carRepository = carRepository;
	}

	/**
	 * Creates a new car
	 * @param shoppingCar {@link ShoppingCar}
	 */
	public ShoppingCar addShoppingCar(ShoppingCar shoppingCar) {
		return carRepository.save(shoppingCar);
	}

	/**
	 * Gets al the cars
	 */
	public List<ShoppingCar> findAllCars(){
		return carRepository.findAll();
	}

	/**
	 * Updates a car
	 * @param shoppingCar {@link ShoppingCar}
	 */
	public ShoppingCar updateShoppingCar(ShoppingCar shoppingCar) {
		return carRepository.save(shoppingCar);
	}

	/**
	 * Deletes a car
	 * @param id
	 */
	public void deleteShoppingCarById(Integer id) {
		carRepository.deleteById(id);
	}

	/**
	 * Get a car by a given ID
	 * @param id
	 */
	public ShoppingCar findShoppingCarById(Integer id) {
		return carRepository.findById(id).orElseThrow(() -> new ProductNotFoundException("car by id: " + id +" not found"));
	}

	/**
	 * Adds a prduct to a specific car
	 * @param car {@link ShoppingCar}
	 * @param product {@link Product}
	 */
	public ShoppingCar addProductToCar(ShoppingCar car, Product product){
		car.setCarPrice(car.getCarPrice()+product.getProductPrice());
		car.getProductsByCar().add(product);
		log.info("The product {} has been added to car  {}",String.valueOf(product.getProductId()), car.getCarId() );
		return carRepository.save(car);
	}

	/**
	 * Deletes a product from a speficic car
	 * @param car {@link ShoppingCar}
	 * @param product {@link Product}
	 */
	public ShoppingCar deleteProductFromCar(ShoppingCar car, Product product){
		car.setCarPrice(car.getCarPrice()-product.getProductPrice());
		car.getProductsByCar().remove(product);
		ShoppingCar response = carRepository.save(car);
		log.info("The product {} has been deleted from car  {}",String.valueOf(product.getProductId()), car.getCarId() );

		return  response;
	}

	/**
	 * Deletes all the products from the car
	 * @param car {@link ShoppingCar}
	 */
	public ShoppingCar clearCar(ShoppingCar car){
		car.setCarPrice(0);
		car.getProductsByCar().clear();
		ShoppingCar response = carRepository.save(car);
		log.info("The car {} has been cleared",String.valueOf(car.getCarId()));
		return  response;
	}

}
