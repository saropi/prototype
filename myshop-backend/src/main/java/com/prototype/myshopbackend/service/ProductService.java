package com.prototype.myshopbackend.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.prototype.myshopbackend.exception.ProductNotFoundException;
import com.prototype.myshopbackend.model.Product;
import com.prototype.myshopbackend.repository.ProductRepository;

/**
 * Product Service
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@Service
public class ProductService {

	/**
	 * Product repository
	 */
	private final ProductRepository productRepo;

	/**
	 * Product service Constructor
	 * @param productRepo {@link ProductRepository}
	 */
	public ProductService(final ProductRepository productRepo) {
		this.productRepo = productRepo;
	}

	/**
	 * Adds a product
	 * @param product {@link Product}
	 */
	public Product addProduct(Product product) {
		return productRepo.save(product);
	}

	/**
	 * Gets all the products in the shop
	 */
	public List<Product> findAllProduct(){
		return productRepo.findAll();
	}

	/**
	 * Updates a product
	 * @param product {@link Product}
	 */
	public Product updateProduct(Product product) {
		return productRepo.save(product);
	}

	/**
	 * Deletes a product given an ID
	 * @param id
	 */
	public void deleteProductById(Integer id) {
		productRepo.deleteById(id);
	}

	/**
	 * Gets a product given an ID
	 * @param id
	 */
	public Product findProductById(Integer id) {
		return productRepo.findById(id).orElseThrow(() -> new ProductNotFoundException("Product by id: " + id +" not found"));
	}
}
