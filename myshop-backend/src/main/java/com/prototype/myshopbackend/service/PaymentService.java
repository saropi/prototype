package com.prototype.myshopbackend.service;

import java.util.List;
import java.util.UUID;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import com.prototype.myshopbackend.adapter.NetworkAdapter;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.PaymentResponse;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundResponse.RefundResponse;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.exception.PaymentNotFoundException;
import com.prototype.myshopbackend.model.Payment;
import com.prototype.myshopbackend.repository.PaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Payment Service
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */

@Service
public class PaymentService {
	Logger log = LoggerFactory.getLogger(PaymentService.class);

	/**
	 * Payment repository
	 * Network adapter
	 */
	private final PaymentRepository paymentRepo;
	private final NetworkAdapter  networkAdapter;

	/**
	 * Payment service Constructor
	 * @param paymentRepo {@link PaymentRepository}
	 * @param networkAdapter {@link NetworkAdapter}
	 */
	public PaymentService(final PaymentRepository paymentRepo ,final NetworkAdapter  networkAdapter ) {
		this.paymentRepo = paymentRepo;
		this.networkAdapter = networkAdapter;
	}

	/**
	 * Adds the payment to DB. Checks if the transaction is ok or not.
	 * Returns a Payment
	 * @param paymentDTO {@link PaymentDTO}
	 */
	public Payment addPayment(PaymentDTO paymentDTO) {

		Payment payment =  new Payment();
		payment.setTotalAmount(paymentDTO.getTotalAmount());
		payment.setUserId(paymentDTO.getUserId());
		UUID uuid = UUID.randomUUID();
		String id = uuid.toString();

		payment.setPaymentUuId(id);
		paymentDTO.setPaymentUuId(id);

		payment.setPaymentStatus("");
		Payment response = paymentRepo.save(payment);
		PaymentResponse paymentResponse = this.networkAdapter.paymentTx(paymentDTO);
		if(paymentResponse != null){
			try {
					payment.setPaymentStatus(paymentResponse.getTransactionResponse().getState());
				payment.setTransactionId(paymentResponse.getTransactionResponse().getTransactionId());
				payment.setPaymentOrderId(paymentResponse.getTransactionResponse().getOrderId());
				log.info("payment by UuId {} has been processed",paymentDTO.getPaymentUuId());
			}catch (NullPointerException e){
				payment.setPaymentStatus(String.valueOf(Payment.PaymentStatus.ERROR));
				paymentRepo.save(payment);
				log.error("Error in api response: {}",paymentResponse.getTransactionResponse().getResponseMessage());
				throw new EmptyResultDataAccessException((String) paymentResponse.getTransactionResponse().getResponseMessage(), 1);
			}
			return paymentRepo.save(response);
		}
		throw new EmptyResultDataAccessException("Error connecting to PayU API by request from user: " + payment.getUserId(), 1);
	}

	/**
	 * Returns all the payments made by a user given a user ID
	 * @param userId
	 */
	public List<Payment> findAllpaymentsByUserId(int userId){
		return paymentRepo.findAllpaymentsByUserId(userId);
	}

	/**
	 * Returns a pyment given a payment ID
	 * @param id
	 */
	public Payment findPaymentById(Integer id) {
		return paymentRepo.findById(id).orElseThrow(() -> new PaymentNotFoundException("Payment by id: " + id +" not found"));
	}

	/**
	 * Makes the refund calling the API and validates the responses
	 * @param payment {@link Payment}
	 */
	public Payment refund(Payment payment){
		RefundResponse refundResponse =  this.networkAdapter.refundTx(payment);
		if("ERROR".equals(refundResponse.getCode())){
			log.warn(refundResponse.getError().toString());
			throw new EmptyResultDataAccessException(refundResponse.getError().toString(), 1);
		}

		log.info("payment by UuId {} has status {}",payment.getPaymentUuId(),refundResponse.getTransactionResponse().getState());
		payment.setPaymentStatus(String.valueOf(Payment.PaymentStatus.REFUNDED));
		return paymentRepo.save(payment);

	}
}
