package com.prototype.myshopbackend.exception;

public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(String msg) {

		super(msg);
	}
}
