package com.prototype.myshopbackend.exception;

//checked and unchecked exceptions
public class PaymentNotFoundException extends RuntimeException {

	public PaymentNotFoundException(String msg) {

		super(msg);
	}

}
