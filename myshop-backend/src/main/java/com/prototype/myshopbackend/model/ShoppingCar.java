package com.prototype.myshopbackend.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Shopping Car representation entity. This represent the shopping car that contains products the user will pay
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@Table(name = "shopping_car")
public class ShoppingCar {

	/**
	 * The class ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "car_id")
	private int carId;

	/**
	 * The sum of the price of each product in the car.
	 */
	@Column(name = "car_total_price", nullable = false)
	private long carPrice;

	/**
	 * Status of the car
	 */
	@Column(name = "car_status", nullable = false, columnDefinition = "varchar(32) default 'OPEN'")
	@Enumerated(EnumType.STRING)
	private CarStatus carStatus;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@JoinTable(
			name = "products_by_car",
			joinColumns = @JoinColumn(name = "product_id"),
			inverseJoinColumns = @JoinColumn(name = "car_id")
	)

	private List<Product> productsByCar = new ArrayList<>();

	public ShoppingCar() {

	}

	public ShoppingCar(final int carId, final long carPrice, final CarStatus carStatus,
					   final List<Product> productsByCar) {

		this.carId = carId;
		this.carPrice = carPrice;
		this.carStatus = carStatus;
		this.productsByCar = productsByCar;
	}

	public int getCarId() {

		return carId;
	}

	public void setCarId(final int carId) {

		this.carId = carId;
	}

	public long getCarPrice() {

		return carPrice;
	}

	public void setCarPrice(final long carPrice) {

		this.carPrice = carPrice;
	}

	public CarStatus getCarStatus() {

		return carStatus;
	}

	public void setCarStatus(final CarStatus carStatus) {

		this.carStatus = carStatus;
	}

	public List<Product> getProductsByCar() {

		return productsByCar;
	}

	@Override public String toString() {

		return "ShoppingCar{" +
				"carId=" + carId +
				", carPrice=" + carPrice +
				", carStatus=" + carStatus +
				", productsByCar=" + productsByCar +
				'}';
	}

	public void setProductsByCar(final List<Product> productsByCar) {

		this.productsByCar = productsByCar;
	}

	public enum CarStatus {
		OPEN, FINISHED;
	}

}
