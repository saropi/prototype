package com.prototype.myshopbackend.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Payment representation entity. This represent the user. It can have admin permission.
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@Table(name = "usuario")
public class User implements Serializable {

	/**
	 * The class ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;

	/**
	 * The name of the user.
	 */
	@Column(name = "user_name", nullable = false, updatable = false)
	private String userName;

	/**
	 * The password of the user.
	 */
	@Column(name = "user_password", nullable = false, updatable = false)
	private String userPassword;

	/**
	 * The document of the user.
	 *
	 * @unic
	 */
	@Column(name = "user_document", unique = true, nullable = false, updatable = false)
	private String userDocument;

	/**
	 * Check if the user is admin.
	 */
	@Column(name = "user_admin")
	private Boolean userAdmin;

	@OneToOne(cascade = CascadeType.ALL, optional = true)
	private ShoppingCar car;

	public User() {

	}

	public User(String userName, String userDocument, String userPassword, Boolean userAdmin, ShoppingCar car) {

		this.userName = userName;
		this.userDocument = userDocument;
		this.userPassword = userPassword;
		this.userAdmin = userAdmin;
		this.car = car;
	}

	public ShoppingCar getCar() {

		return car;
	}

	public void setCar(final ShoppingCar car) {

		this.car = car;
	}

	public int getUserId() {

		return userId;
	}

	public void setUserId(int userId) {

		this.userId = userId;
	}

	public String getUserName() {

		return userName;
	}

	public void setUserName(String userName) {

		this.userName = userName;
	}

	public String getUserPassword() {

		return userPassword;
	}

	public void setUserPassword(String password) {

		this.userPassword = password;
	}

	public String getUserDocument() {

		return userDocument;
	}

	public void setUserDocument(String userDocument) {

		this.userDocument = userDocument;
	}

	public Boolean getUserAdmin() {

		return userAdmin;
	}

	public void setUserAdmin(Boolean userAdmin) {

		this.userAdmin = userAdmin;
	}

}
