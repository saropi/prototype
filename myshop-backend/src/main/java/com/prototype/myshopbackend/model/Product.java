package com.prototype.myshopbackend.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Product representation entity. This represent the product in the shop.
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "cars"})
@Table(name = "product")
public class Product implements Serializable {

	/**
	 * The class ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	private int productId;

	/**
	 * The name of the product.
	 */
	@Column(name = "product_name", nullable = false)
	private String productName;

	/**
	 * The production of the product.
	 */
	@Column(name = "product_description", nullable = false)
	private String productDescription;

	/**
	 * The image of the product.
	 */
	@Column(name = "image", length = 100000)
	private byte[] productImage;

	/**
	 * The production of the product.
	 */
	@Column(name = "product_price", nullable = false)
	private long productPrice;

	/**
	 * The car that the product is in.
	 */
	@ManyToMany(mappedBy = "productsByCar")
	private List<ShoppingCar> cars = new ArrayList<>();

	public Product() {

	}

	public Product(final int productId, final String productName, final String productDescription, final byte[] productImage,
				   final long productPrice, final List<ShoppingCar> cars) {

		this.productId = productId;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productImage = productImage;
		this.productPrice = productPrice;
		this.cars = cars;
	}

	public int getProductId() {

		return productId;
	}

	public void setProductId(final int productId) {

		this.productId = productId;
	}

	public String getProductName() {

		return productName;
	}

	public void setProductName(final String productName) {

		this.productName = productName;
	}

	public String getProductDescription() {

		return productDescription;
	}

	public void setProductDescription(final String productDescription) {

		this.productDescription = productDescription;
	}

	public byte[] getProductImage() {

		return productImage;
	}

	public void setProductImage(final byte[] productImage) {

		this.productImage = productImage;
	}

	public List<ShoppingCar> getCars() {

		return cars;
	}

	public void setCars(final List<ShoppingCar> cars) {

		this.cars = cars;
	}

	public long getProductPrice() {

		return productPrice;
	}

	public void setProductPrice(final long productPrice) {

		this.productPrice = productPrice;
	}
}
