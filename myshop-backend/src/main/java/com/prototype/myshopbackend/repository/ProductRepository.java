package com.prototype.myshopbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.prototype.myshopbackend.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
