package com.prototype.myshopbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.prototype.myshopbackend.model.ShoppingCar;

public interface ShoppingCarRepository extends JpaRepository<ShoppingCar, Integer> {

}
