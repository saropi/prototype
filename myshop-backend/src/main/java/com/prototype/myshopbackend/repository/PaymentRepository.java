package com.prototype.myshopbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.prototype.myshopbackend.model.Payment;
import com.prototype.myshopbackend.model.User;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

	@Query(value = "SELECT * FROM payment where user_id = ?1 ", nativeQuery = true)
	List<Payment> findAllpaymentsByUserId(int userId);
}
