package com.prototype.myshopbackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.prototype.myshopbackend.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query(value = "SELECT * FROM usuario where user_document = ?1 ", nativeQuery = true)
	User findByUserDocument(String document);
}
