package com.prototype.myshopbackend.dto;

/**
 * This class represents a Payment data transfer object.
 *
 * @author Santiago Romero Pineda (santiago.romero@pay.com)
 * @version 1.0.0
 * @since 1.1.0
 */

public class PaymentDTO {

	/**
	 * The id of the user is making the payment
	 */
	private int userId;

	/**
	 * Total amount the user pays
	 */
	private long totalAmount;

	/**
	 * Number of the credit card
	 */

	private String creditCardNumber;

	/**
	 * Security code of the credit card
	 */
	private String securityCode;

	/**
	 * Expiration date of the credit card
	 */
	private String expirationDate;

	/**
	 * Owner's name of the credit card
	 */
	private String creditCardName;

	/**
	 * The UId of the payment
	 */
	private String paymentUuId;

	public PaymentDTO() {

	}

	;

	public PaymentDTO(final int userId, final long totalAmount, final String creditCardNumber,
					  final String securityCode,
					  final String expirationDate, final String paymentUuId, final String creditCardName) {

		this.userId = userId;
		this.totalAmount = totalAmount;
		this.creditCardNumber = creditCardNumber;
		this.securityCode = securityCode;
		this.expirationDate = expirationDate;
		this.creditCardName = creditCardName;
		this.paymentUuId = paymentUuId;
	}

	public int getUserId() {

		return userId;
	}

	public void setUserId(final int userId) {

		this.userId = userId;
	}

	public String getPaymentUuId() {

		return paymentUuId;
	}

	public void setPaymentUuId(final String paymentUuId) {

		this.paymentUuId = paymentUuId;
	}

	public long getTotalAmount() {

		return totalAmount;
	}

	public void setTotalAmount(final long totalAmount) {

		this.totalAmount = totalAmount;
	}

	public String getCreditCardNumber() {

		return creditCardNumber;
	}

	public void setCreditCardNumber(final String creditCardNumber) {

		this.creditCardNumber = creditCardNumber;
	}

	public String getSecurityCode() {

		return securityCode;
	}

	public void setSecurityCode(final String securityCode) {

		this.securityCode = securityCode;
	}

	public String getExpirationDate() {

		return expirationDate;
	}

	public void setExpirationDate(final String expirationDate) {

		this.expirationDate = expirationDate;
	}

	public String getCreditCardName() {

		return creditCardName;
	}

	public void setCreditCardName(final String creditCardName) {

		this.creditCardName = creditCardName;
	}
}
