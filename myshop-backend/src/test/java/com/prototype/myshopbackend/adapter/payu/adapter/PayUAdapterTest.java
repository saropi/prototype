package com.prototype.myshopbackend.adapter.payu.adapter;

import java.lang.reflect.Field;
import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.junit.Before;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.PaymentResponse;
import com.prototype.myshopbackend.adapter.payu.DTO.refund.refundResponse.RefundResponse;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.model.Payment;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@ExtendWith(MockitoExtension.class)
class PayUAdapterTest {

	@Mock
	private PaymentDTO paymentDTO;

	@Mock
	private RestTemplate restTemplate;



	@InjectMocks
	private PayUAdapter payUAdapter;

	@Before
	public void setUp() throws NoSuchFieldException, IllegalAccessException {

		MockitoAnnotations.openMocks(this);
		final Field payUURLField = this.payUAdapter.getClass().getDeclaredField("payuUrl");
		payUURLField.setAccessible(true);
		payUURLField.set(payUAdapter, "https://sandbox.api.payulatam.com/payments-api/4");
	}

	@Test
	public void paymentTxTest_good_request() {

		this.setPaymentDTOCorrectData();
		final String payuUrl = "https://sandbox.api.payulatam.com/payments-api/4";

		when(restTemplate.postForEntity(any(String.class), any(Object.class), any(Class.class))).thenReturn(
				this.setSuccessPaymentResponse());

		PaymentResponse paymentResponse = this.payUAdapter.paymentTx(this.paymentDTO);
		//verify(restTemplate).postForEntity(eq(payuUrl), any(Object.class), any(Class.class));

		Assertions.assertEquals("SUCCESS", paymentResponse.getCode());
	}

	@Test
	public void paymentTx_bad_request() {

		this.setPaymentDTOIncorrectData();

		when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class))).thenReturn(this
																														.setErrorPaymentResponse());

		PaymentResponse paymentResponse = this.payUAdapter.paymentTx(this.paymentDTO);

		Assertions.assertEquals("ERROR", paymentResponse.getCode());

	}

	@Test
	public void refundTx_good_request() {

		when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class))).thenReturn(
				this.setSuccessRefundResponse());

		RefundResponse refundResponse = this.payUAdapter.refundTx(this.setCorrectPayment());

		Assertions.assertEquals("SUCCESS", refundResponse.getCode());

	}

	@Test
	public void refundTx_bad_request() {

		when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class))).thenReturn(this
																														.setErrorRefundResponse());

		RefundResponse refundResponse = this.payUAdapter.refundTx(this.setIncorrectPayment());

		Assertions.assertEquals("ERROR", refundResponse.getCode());

	}





	private void setPaymentDTOCorrectData() {

		this.paymentDTO = new PaymentDTO();
		this.paymentDTO.setTotalAmount(15000);
		this.paymentDTO.setCreditCardNumber("4097440000000004");
		this.paymentDTO.setCreditCardName("APPROVED");
		this.paymentDTO.setExpirationDate("2021/12");
		this.paymentDTO.setSecurityCode("777");
		this.paymentDTO.setUserId(1);
	}

	private void setPaymentDTOIncorrectData() {

		this.paymentDTO = new PaymentDTO();
		this.paymentDTO.setTotalAmount(15000);
		this.paymentDTO.setCreditCardNumber("400000004");
		this.paymentDTO.setCreditCardName("Juan");
		this.paymentDTO.setExpirationDate("2021/12");
		this.paymentDTO.setSecurityCode("777");
		this.paymentDTO.setUserId(1);
	}

	private ResponseEntity<PaymentResponse> setErrorPaymentResponse() {

		PaymentResponse pr = new PaymentResponse();
		ResponseEntity<PaymentResponse> response = new ResponseEntity<PaymentResponse>(pr, BAD_REQUEST);
		response.getBody().setCode("ERROR");
		return response;
	}

	private ResponseEntity<PaymentResponse> setSuccessPaymentResponse() {

		PaymentResponse pr = new PaymentResponse();
		ResponseEntity<PaymentResponse> response = new ResponseEntity<PaymentResponse>(pr, OK);
		response.getBody().setCode("SUCCESS");
		return response;
	}

	private ResponseEntity<RefundResponse> setErrorRefundResponse() {

		RefundResponse pr = new RefundResponse();
		ResponseEntity<RefundResponse> response = new ResponseEntity<RefundResponse>(pr, BAD_REQUEST);
		response.getBody().setCode("ERROR");
		return response;
	}

	private ResponseEntity<RefundResponse> setSuccessRefundResponse() {

		RefundResponse pr = new RefundResponse();
		ResponseEntity<RefundResponse> response = new ResponseEntity<RefundResponse>(pr, OK);
		response.getBody().setCode("SUCCESS");
		return response;
	}

	private Payment setCorrectPayment() {

		Payment payment = new Payment();
		payment.setUserId(1);
		payment.setPaymentOrderId(1234342L);
		payment.setTotalAmount(15000);
		payment.setPaymentDate(new Date());
		payment.setPaymentUuId("wefubewuy3475-34r4fr");
		payment.setPaymentStatus("APPROVED");
		payment.setPaymentId(1);
		payment.setTransactionId("42ihvrt4vf3kuvf3uf3-f3uvr3tiveuy");
		return payment;
	}

	private Payment setIncorrectPayment() {

		Payment payment = new Payment();
		payment.setUserId(1);
		payment.setPaymentOrderId(1234342L);
		payment.setTotalAmount(15000);
		payment.setPaymentDate(new Date());
		payment.setPaymentUuId("wefubewuy3475-34r");
		payment.setPaymentStatus("APPROVED");
		payment.setPaymentId(1);
		payment.setTransactionId("42ihvrt4vf3kuvf3uf3-f3uvr3uy");
		return payment;
	}

}