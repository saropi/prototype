package com.prototype.myshopbackend.service;

import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.PaymentResponse;
import com.prototype.myshopbackend.adapter.payu.DTO.payment.paymentResponse.TransactionResponse;
import com.prototype.myshopbackend.adapter.payu.adapter.PayUAdapter;
import com.prototype.myshopbackend.dto.PaymentDTO;
import com.prototype.myshopbackend.model.Payment;
import com.prototype.myshopbackend.repository.PaymentRepository;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

	@InjectMocks
	private PaymentService paymentService;

	@Mock
	private PaymentRepository paymentRepo;

	@Mock
	private PayUAdapter payUAdapter;

	@Test
	public void addPayment_Success() {

		when(paymentRepo.save(ArgumentMatchers.any(Payment.class))).thenReturn(this.setPayment());
		when(payUAdapter.paymentTx(ArgumentMatchers.any(PaymentDTO.class))).thenReturn(this.setPaymentResponse());

		Payment response = this.paymentService.addPayment(this.setPaymentDTO());
		Assertions.assertEquals(response.getPaymentStatus(), "APPROVED");
		Assertions.assertEquals(response.getTransactionId(), "42ihvrt4vf3kuvf3uf3-f3uvr3tiveuy");

	}

	private PaymentDTO setPaymentDTO(){
		PaymentDTO paymentDTO = new PaymentDTO();
		paymentDTO.setTotalAmount(15000);
		paymentDTO.setCreditCardNumber("4097440000000004");
		paymentDTO.setCreditCardName("APPROVED");
		paymentDTO.setExpirationDate("2021/12");
		paymentDTO.setSecurityCode("777");
		paymentDTO.setUserId(1);
		return paymentDTO;
	}

	private Payment setPayment(){
		Payment payment = new Payment();
		payment.setUserId(1);
		payment.setPaymentOrderId(1234342L);
		payment.setTotalAmount(15000);
		payment.setPaymentDate(new Date());
		payment.setPaymentUuId("wefubewuy3475-34r4fr");
		payment.setPaymentStatus("APPROVED");
		payment.setPaymentId(1);
		payment.setTransactionId("42ihvrt4vf3kuvf3uf3-f3uvr3tiveuy");
		return payment;
	}

	private PaymentResponse setPaymentResponse(){
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setTransactionResponse(new TransactionResponse());
		paymentResponse.getTransactionResponse().setState("APPROVED");
		paymentResponse.getTransactionResponse().setTransactionId("42ihvrt4vf3kuvf3uf3-f3uvr3tiveuy");
		paymentResponse.getTransactionResponse().setOrderId(1234342L);
		return paymentResponse;
	}
}