import { ShoppingCar } from "./shopping-car";

export class User {
    userId:  number | undefined ;
    userName: string | undefined ;
    userPassword: string | undefined ;
    userDocument: string | undefined ;
    userAdmin: boolean | undefined ;
    car : ShoppingCar | undefined;
    constructor(){}
}
 