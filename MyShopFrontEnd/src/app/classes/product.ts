import { ShoppingCar } from "./shopping-car";

export class Product {
    productId:  number | undefined ;
    productName: string | undefined ;
    productDescription: string | undefined ;
    productImage : File | undefined;
    productPrice : number | undefined;
    cars : ShoppingCar[] | undefined; 

    constructor(){}
}
