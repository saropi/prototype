import { Product } from "./product";

export class ShoppingCar {
    carId:  number | undefined ;
    carPrice: number | undefined ;
    carStatus: string | undefined ;
    productsByCar : Array<Product>=[];
    constructor(){}
}
