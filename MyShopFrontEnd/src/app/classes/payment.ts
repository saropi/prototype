export class Payment {
    paymentId:  number | undefined ;
    paymentUuId: string | undefined;
    userId:  number | undefined ;
    paymentDate:  Date | undefined ;
    totalAmount: number | undefined ;
    paymentStatus: string | undefined ;
    transactionId: string | undefined ;
    paymentOrderId: string | undefined;
    constructor(){}
}
