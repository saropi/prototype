import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../classes/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User();
  msg = '';

  constructor(private _service: RegistrationService, private _router: Router) { }

  ngOnInit(): void {
  }

  loginUser() {
    this._service.loginUserFromRemote(this.user).subscribe(
      data => {

        console.log("login");
        this._router.navigate(['/login-success'], { state: {user:data}});
      },
      error => {
        console.log("error");
        this.msg = "Bad credentials, please enter a valid document and password";
      }
    );
  }
  gotToRegistration(){
    this._router.navigate(['/registration']);
  }
}
