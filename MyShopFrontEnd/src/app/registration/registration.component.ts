import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../classes/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  user = new User();
  msg = '';

  constructor(private _service: RegistrationService, private _router: Router) { }

  ngOnInit(): void {
  }
  addUser() {
    this._service.addUser(this.user).subscribe(
      data => {
        this.user=data;
        console.log("usuario encontrado");
        this._router.navigate(['/login-success'], { state: {user:data}});
      },
      error => {
        console.log("error");
        this.msg = "The user is already registered or any field is not filled yet";
      }
    );
  }

}
