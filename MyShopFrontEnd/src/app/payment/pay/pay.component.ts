import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Paymentdto } from 'src/app/classes/paymentdto';
import { Product } from 'src/app/classes/product';
import { ShoppingCar } from 'src/app/classes/shopping-car';
import { User } from 'src/app/classes/user';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  user = new User();
  product = new Product();
  car = new ShoppingCar();
  paymentDTO = new Paymentdto();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) {
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
      this._service.createCar(this.user, this.car).subscribe(
        data => {
          this.car = data.car;
          console.log(this.car);
        },
        error => {
          console.log("error Creating or getting the car");
        }
      );
    }
  }

  ngOnInit(): void {
  }

  pay() {
    if ((this.paymentDTO.creditCardName == undefined || this.paymentDTO.creditCardNumber == undefined || this.paymentDTO.expirationDate == undefined || this.paymentDTO.securityCode == undefined)) {
      alert("fill all the inputs CORRECTLY");
    }
    else {
      this.paymentDTO.userId = this.user.userId;
      this.paymentDTO.totalAmount = this.car.carPrice;
      this._service.pay(Number(this.car.carId), this.paymentDTO).subscribe(
        data => {
          alert("payment registered");
          this.backToPayment();
        },
        error => {
          alert("error processing payment \n check your data");
        }
      );
      

    }

  }

  backToPayment() {
    this._router.navigate(['/carlist'], { state: { user: this.user } });
  }
}
