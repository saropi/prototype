import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Payment } from 'src/app/classes/payment';
import { Product } from 'src/app/classes/product';
import { ShoppingCar } from 'src/app/classes/shopping-car';
import { User } from 'src/app/classes/user';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-paymentlist',
  templateUrl: './paymentlist.component.html',
  styleUrls: ['./paymentlist.component.css']
})
export class PaymentlistComponent implements OnInit {

  payments: Array<Payment> = [];
  car = new ShoppingCar();
  user = new User();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
      this._service.createCar(this.user, this.car).subscribe(
        data => {
          this.car = data.car;
        },
        error => {
          console.log("error Creating car");
        }
      );
    }
    
    this._service.getPayments(Number(this.user.userId)).subscribe(
      data =>{
        console.log(data);
        this.payments=data;
      },
      error =>{
        console.log("error loading data")
      }
    );
   }
  ngOnInit(): void {
  }
  goToProducts(){
    this._router.navigate(['/login-success'], { state: {user:this.user}});
  }
  goToCrudProduct(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
  }
  goToMyCar(){
    this._router.navigate(['/carlist'], { state: {user:this.user}});
  }
  goToPay(){
    if(this.car.productsByCar.length == 0){
      alert("Don't have any product in the car")
    }
    else{
      this._router.navigate(['/pay'], { state: {user:this.user}});
    }
  }
  goToPaymentList(){
    this._router.navigate(['/paymentlist'], { state: {user:this.user}});
  }

  refund(index:any , payment: any) {  
    if(payment.paymentStatus=="PENDING"){
      alert("The refund is in process")
    }
    else{
      this._service.refund(payment).subscribe(
        data => {
          console.log("Refund done");
          alert("refund in process");
          this.payments[index]=data;
          
        },
        error => {
          console.log("could not refund");
          alert("Could not refund");
        }
      );
    } 
    
  }

  
}
