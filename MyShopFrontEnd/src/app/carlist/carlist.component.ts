import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../classes/product';
import { ShoppingCar } from '../classes/shopping-car';
import { User } from '../classes/user';
import { RegistrationService } from '../registration.service';

@Component({
  selector: 'app-carlist',
  templateUrl: './carlist.component.html',
  styleUrls: ['./carlist.component.css']
})
export class CarlistComponent implements OnInit {
  products: Array<Product> = []
  user = new User();
  car = new ShoppingCar();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
    }
    this._service.createCar(this.user, this.car).subscribe(
      data => {
        this.car = data.car;
      },
      error => {
        console.log("error Creating car");
      }
    );
   }
  ngOnInit(): void {
  }
  goToProducts(){
    this._router.navigate(['/login-success'], { state: {user:this.user}});
  }
  goToCrudProduct(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
  }
  goToMyCar(){
    this._router.navigate(['/carlist'], { state: {user:this.user}});
  }
  goToPay(){
    if(this.car.productsByCar.length == 0){
      alert("Don't have any product in the car")
    }
    else{
      this._router.navigate(['/pay'], { state: {user:this.user}});
    }
  }
  goToPaymentList(){
    this._router.navigate(['/paymentlist'], { state: {user:this.user}});
  }

  deleteProduct(index:any ,car: any,product: any) {    
    console.log(this.car);
    this._service.deleteProductFromCar(Number(car.carId), product).subscribe(
      data => {
        console.log("product deleted");
        this.car.productsByCar.splice(index, 1);
        
        
        this._router.navigate(['/carlist'], { state: {user:this.user}});
      },
      error => {
        console.log("error adding product");
        alert("Could not delete the product");
      }
    );
  }


}
