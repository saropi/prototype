import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../classes/product';
import { ShoppingCar } from '../classes/shopping-car';
import { User } from '../classes/user';
import { RegistrationService } from '../registration.service';

@Component({
  selector: 'app-login-succes',
  templateUrl: './login-succes.component.html',
  styleUrls: ['./login-succes.component.css']
})
export class LoginSuccesComponent implements OnInit {
  products: Product[] = []
  user = new User();
  car = new ShoppingCar();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    this._service.getAllProducts().subscribe(
      data => {
        console.log("Getting products");
        this.products = data;
        console.log(data);
      },
      error => {
        console.log("error Creating car");
      }
    );
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
      this.car.carPrice = 0;
      this.car.carStatus = "OPEN"
      this._service.createCar(this.user, this.car).subscribe(
        data => {
          console.log("Car created");
          this.car = data.car;
          console.log(this.car);
        },
        error => {
          console.log("error Creating car");
        }
      );
    }
   }
  
  ngOnInit(): void {

  }
  goToProducts(){
    this._router.navigate(['/login-success'], { state: {user:this.user}});
  }
  goToCrudProduct(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
  }
  goToMyCar(){
    this._router.navigate(['/carlist'], { state: {user:this.user}});
  }
  addProduct(car: any,product: any) {    
    console.log(this.car);
    this._service.addProductToCar(Number(car.carId), product).subscribe(
      data => {
        console.log("Product added");
        alert("product added");
        
      },
      error => {
        console.log("error adding product");
        alert("Could not add the product");
      }
    );
  }
}
