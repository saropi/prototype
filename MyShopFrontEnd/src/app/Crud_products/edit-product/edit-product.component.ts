import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/classes/product';
import { User } from 'src/app/classes/user';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  user = new User();
  product = new Product();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
      this.product = this.routeState.product;
    }
   }

   updateProduct(){
    this._service.updateProduct(this.product).subscribe(
      data => {
        this.product=data;
        console.log("product updated");
        this._router.navigate(['/productlist'], { state: {user:this.user}});
      },
      error => {
        console.log("does not update");
      }
    );

   }


   cancelEdition(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
   }

  ngOnInit(): void {
  }

}
