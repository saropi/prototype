import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/classes/product';
import { ShoppingCar } from 'src/app/classes/shopping-car';
import { User } from 'src/app/classes/user';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  products: Array<Product> = []
  user = new User();
  car = new ShoppingCar();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    this._service.getAllProducts().subscribe(
      data => {
        console.log("Getting products");
        this.products = data;
        console.log(data);
      },
      error => {
        console.log("Erro getting products");
      }
    );
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
      this.car.carPrice = 0;
      this.car.carStatus = "OPEN"
    }
   }
  

  ngOnInit(): void {
  } 
  goToProducts(){
    this._router.navigate(['/login-success'], { state: {user:this.user}});
  }
  goToCrudProduct(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
  }
  goToMyCar(){
    this._router.navigate(['/carlist'], { state: {user:this.user}});
  }
  goToEditProduct(product : any){
    this._router.navigate(['/edit-product'], { state: {user:this.user, product: product}});
  }

  
  deleteProduct(index:any ,product: any) {    
    console.log(this.car);
    this._service.deleteProduct(Number(product)).subscribe(
      data => {
        console.log("product deleted");
        this.products.splice(index, 1);        
      },
      error => {
        console.log("error adding product");
        alert("Could not delete the product");
      }
    );
  }

  addProduct(){
    this._router.navigate(['/createproduct'], { state: {user:this.user}});
  }



}
