import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/classes/product';
import { User } from 'src/app/classes/user';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-createproduct',
  templateUrl: './createproduct.component.html',
  styleUrls: ['./createproduct.component.css']
})
export class CreateproductComponent implements OnInit {

  user = new User();
  product = new Product();
  routeState: any;

  constructor(private _router: Router, private _service: RegistrationService) { 
    console.log(this._router.getCurrentNavigation())
    if (this._router.getCurrentNavigation()?.extras.state) {
      this.routeState = this._router.getCurrentNavigation()?.extras.state;
    }
    if (this.routeState) {
      this.user = this.routeState.user;
    }
   }

  ngOnInit(): void {
  }

  addProduct(){
    this._service.crateProduct(this.product).subscribe(
      data => {
        this.product=data;
        console.log("product created");
        this._router.navigate(['/productlist'], { state: {user:this.user}});
      },
      error => {
        console.log("could not create");
      }
    );
  }

  cancelAdding(){
    this._router.navigate(['/productlist'], { state: {user:this.user}});
   }
}
