import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginSuccesComponent } from './login-succes/login-succes.component';
import { ProductlistComponent } from './Crud_products/productlist/productlist.component';
import { EditProductComponent } from './Crud_products/edit-product/edit-product.component';
import { CarlistComponent } from './carlist/carlist.component';
import { CreateproductComponent } from './Crud_products/createproduct/createproduct.component';
import { PaymentlistComponent } from './payment/paymentlist/paymentlist.component';
import { PayComponent } from './payment/pay/pay.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    LoginSuccesComponent,
    ProductlistComponent,
    EditProductComponent,
    CarlistComponent,
    CreateproductComponent,
    PaymentlistComponent,
    PayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
