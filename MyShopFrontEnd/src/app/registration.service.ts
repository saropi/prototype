import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './classes/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShoppingCar } from './classes/shopping-car';
import { Product } from './classes/product';
import { Payment } from './classes/payment';
import { Paymentdto } from './classes/paymentdto';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Access-Control-Allow-Headers', 'Content-Type')
      .append('Access-Control-Allow-Methods', '*')
      .append('Access-Control-Allow-Origin', '*');

  constructor(private _http: HttpClient) { }

  public loginUserFromRemote(user: User): Observable<any> {
    return this._http.post<any>("http://localhost:8081/user/login", user);
  }
  public addUser(user: User): Observable<any> {
    return this._http.post<any>("http://localhost:8081/user/addUser", user);
  }
  public createCar(user: User, car: ShoppingCar): Observable<any> {
    return this._http.post<any>("http://localhost:8081/user/addCar/" + user.userId, car);
  }
  public getAllProducts(): Observable<any> {
    return this._http.get<any[]>("http://localhost:8081/product/all/");
  }
  public addProductToCar(carId: number, product: Product): Observable<any> {
    return this._http.post<any[]>("http://localhost:8081/shoppingCar/addProductToCar/" + carId, product);
  }
  public deleteProductFromCar(carId: number, product: Product): Observable<any> {
    return this._http.post<any[]>("http://localhost:8081/shoppingCar/deleteProductFromCar/" + carId, product);
  }
  public deleteProduct(productId: number): Observable<any> {
    return this._http.get<any>("http://localhost:8081/product/deleteProduct/" + productId);
  }
  public updateProduct( product: Product): Observable<any> {
    return this._http.post<any>("http://localhost:8081/product/addProduct", product);
  }
  public crateProduct( product: Product): Observable<any> {
    return this._http.post<any>("http://localhost:8081/product/updateProduct", product);
  }
  public pay(carId : number, paymentDto: Paymentdto): Observable<any> {
    return this._http.post<any>("http://localhost:8081/payment/pay/"+carId,paymentDto);
  }
  public getPayments(userId: number): Observable<any> {
    return this._http.get<any[]>("http://localhost:8081/payment/all/"+userId);
  }

  public refund(payment :Payment): Observable<any> {
    return this._http.post<any>("http://localhost:8081/payment/refund",payment);
  }

  

}
