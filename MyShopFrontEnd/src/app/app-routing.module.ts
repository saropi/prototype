import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarlistComponent } from './carlist/carlist.component';
import { CreateproductComponent } from './Crud_products/createproduct/createproduct.component';
import { EditProductComponent } from './Crud_products/edit-product/edit-product.component';
import { ProductlistComponent } from './Crud_products/productlist/productlist.component';
import { LoginSuccesComponent } from './login-succes/login-succes.component';
import { LoginComponent } from './login/login.component';
import { PayComponent } from './payment/pay/pay.component';
import { PaymentlistComponent } from './payment/paymentlist/paymentlist.component';
import { RegistrationComponent } from  './registration/registration.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'login-success',component:LoginSuccesComponent},
  {path:'registration',component:RegistrationComponent},
  {path:'carlist',component: CarlistComponent},
  {path:'productlist',component: ProductlistComponent},
  {path:'edit-product',component: EditProductComponent},
  {path:'createproduct',component: CreateproductComponent},
  {path:'pay',component: PayComponent},
  {path:'paymentlist',component: PaymentlistComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
